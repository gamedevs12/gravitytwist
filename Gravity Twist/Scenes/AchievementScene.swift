//
//  AchievementScene.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 6/20/21.
//  Copyright © 2021 Narv. All rights reserved.
//

import SpriteKit

class AchievementScene: SKScene {
    
    var sceneManagerDelegate: SceneManagerDelegate?
    var popUpUI: PopupUI?
    var creditsButton = SpriteKitButton()
    var supportButton = SpriteKitButton()
    
    
    init(size: CGSize, sceneManagerDelegate: SceneManagerDelegate) {
        self.sceneManagerDelegate = sceneManagerDelegate
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMove(to view: SKView) {
        // Focus with background
        addBackground()
        addMusic()
        self.run(SKAction.wait(forDuration: 2)) {
            self.addCreditsButton()
            self.addHomeButton()
        }
    }
    
    func addMusic() {
        Audio.changeToVictoryMusic()
    }
    
    func addBackground() {
        let bg = SKSpriteNode(imageNamed: "AchievementBackground")
        bg.position = CGPoint(x: frame.midX, y: frame.midY)
        bg.zPosition = GameConstants.ZPositions.background
        addChild(bg)
    }
    
    func addCreditsButton() {
        let btn = SpriteKitButton(defaultImageName: "OptionMenu_Button",action: handleButton, index: 0)
        btn.setText(text: "Credits")
        btn.zPosition = GameConstants.ZPositions.hudFront
        btn.position = CGPoint(x: frame.minX+btn.size.width, y: frame.maxY-btn.size.height)
        addChild(btn)
    }
    
    func addHomeButton() {
        let btn = SpriteKitButton(defaultImageName: "OptionMenu_Button",action: handleButton, index: 1)
        btn.setText(text: "Home")
        btn.zPosition = GameConstants.ZPositions.hudFront
        btn.position = CGPoint(x: frame.maxX-btn.size.width, y: frame.maxY-btn.size.height)
        addChild(btn)
    }
    
    func handleButton(idx: Int) {
        switch(idx) {
        case 0:
            sceneManagerDelegate?.presentCreditsScene()
        case 1:
            sceneManagerDelegate?.presentMenuScene()
        default:
            break
        }
    }
}
