//
//  WorldSelectScene.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 3/4/19.
//  Copyright © 2019 Narv. All rights reserved.
//

import SpriteKit

class WorldSelectScene: SKScene {
    
    var sceneManagerDelegate: SceneManagerDelegate?
    var sceneIndex: Int
    var sceneSubs: [WorldSceneSub]
    var currentSubScene: Int?
    
    init(size: CGSize, startIndex: Int,sceneManagerDelegate: SceneManagerDelegate) {
        self.sceneManagerDelegate = sceneManagerDelegate
        self.sceneIndex = startIndex
        self.sceneSubs = [WorldSceneSub]()
        super.init(size: size)
        addBackground()
        setupSubScenes()
        loadSceneSub()
        loadTransitionButtons()
        Audio.changeToMenuMusic()
    }
    
    func addBackground() {
        let background = SKSpriteNode(imageNamed: "World_Menu-16-9")
        background.position = CGPoint(x: frame.midX, y: frame.midY)
        background.scale(to: frame.size)
        background.zPosition = GameConstants.ZPositions.background
        self.addChild(background)
    }
    
    func setupSubScenes() {
        let subIndex = GameConstants.WorldConstants.totalWorldSubs - 1;
        for i in 0...subIndex
        {
            self.sceneSubs.append(WorldSceneSub(size: frame.size, delegate: self.sceneManagerDelegate!, index: i))
        }
        
        for x in sceneSubs {
            x.anchorPoint = CGPoint(x: 0, y: 0)  // need to do this to center the subscene
            addChild(x)
        }
    }
    
    func loadSceneSub() {
        sceneSubs[sceneIndex].loadLayout(buttonAction: goToLevel)
    }
    
    func clearSceneSub() {
        sceneSubs[sceneIndex].clearLayout()
    }
    
    func loadTransitionButtons() {
        let leftButton = SpriteKitButton(defaultImageName: "LeftArrow.png", action: transition, index: 0)
        leftButton.position = CGPoint(x: frame.maxX*0.08, y: frame.midY)
        leftButton.scale(to: frame.size, width: true, multiplier: 0.06)
        leftButton.zPosition = GameConstants.ZPositions.hudFront
        let rightButton = SpriteKitButton(defaultImageName: "RightArrow.png", action: transition, index: 1)
        rightButton.position = CGPoint(x: frame.maxX*0.92, y: frame.midY)
        rightButton.scale(to: frame.size, width: true, multiplier: 0.06)
        rightButton.zPosition = GameConstants.ZPositions.hudFront
        
        addChild(leftButton)
        addChild(rightButton)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMove(to view: SKView) {
        let siteButton = SpriteKitButton(defaultImageName: GameConstants.StringConstants.optionButtonName, action: openLink, index: 0)
        siteButton.zPosition = GameConstants.ZPositions.hudFront
        siteButton.scale(to: frame.size, width: true, multiplier: 0.2)
        siteButton.position = CGPoint(x: frame.maxX - siteButton.size.width, y: frame.maxY - siteButton.size.height)
        siteButton.setText(text: "Future Updates")
        addChild(siteButton)
        
        let review = SpriteKitButton(defaultImageName: GameConstants.StringConstants.optionButtonName, action: writeAReview, index: 0)
        review.zPosition = GameConstants.ZPositions.hudFront
        review.scale(to: frame.size, width: true, multiplier: 0.2)
        review.position = CGPoint(x: frame.minX + review.size.width, y: frame.maxY - review.size.height)
        review.setText(text: "Rate the Game")
        addChild(review)
        
        let reportBug = SpriteKitButton(defaultImageName: GameConstants.StringConstants.optionButtonName, action: reportBugs, index: 0)
        reportBug.zPosition = GameConstants.ZPositions.hudFront
        reportBug.scale(to: frame.size, width: true, multiplier: 0.2)
        reportBug.position = CGPoint(x: frame.midX, y: frame.maxY - reportBug.size.height)
        reportBug.setText(text: "Report Bugs")
        addChild(reportBug)
    }
    
    func openLink(index: Int) {
        if let url = URL(string: GameConstants.StringConstants.siteLink) {
            UIApplication.shared.open(url, options: [:]) { (success) in
                debugPrint(success)
            }
        }
    }
    
    func writeAReview(index: Int) {
        if let url = URL(string: GameConstants.StringConstants.rateLink) {
            UIApplication.shared.open(url, options: [:]) { (success) in
                debugPrint(success)
            }
        }
    }
    
    func reportBugs(index: Int) {
        if let url = URL(string: GameConstants.StringConstants.bugReportLink) {
            UIApplication.shared.open(url, options: [:]) { (success) in
                debugPrint(success)
            }
        }
    }
    
    func handleButton(idx: Int)
    {
        debugPrint("button: "+String(describing: idx))
    }
    
    func transition(idx: Int) {
        switch(idx) {
            case 0:
                debugPrint("prev scene Index: \(sceneIndex)")
                if sceneIndex == 0 {
                    return
                }
                clearSceneSub()
                sceneIndex = sceneIndex - 1
            case 1:
                if sceneIndex == GameConstants.WorldConstants.totalWorldSubs - 1 {
                    return
                }
                debugPrint("next scene Index: \(sceneIndex)")
                clearSceneSub()
                sceneIndex = sceneIndex + 1
            default:
                debugPrint("Unsupported Button")
        }
        loadSceneSub()
    }
    
    func goToLevel(idx: Int) {
        if(idx > GameConstants.WorldConstants.totalSupportedWorlds)
        {
            debugPrint("World not Implemented yet!")
            return
        }
        if let _ = UserDefaults.standard.value(forKey: GameConstants.Tutorial.introSaveKey) {
            sceneManagerDelegate?.presentLevelScene(for: 1, in: idx)
        } else {
            sceneManagerDelegate?.presentGameScene(for: 0, in: 1)
        }
    }
    
    func presentWorldScene() {
        
    }
}
