//
//  MenuScene.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 2/26/19.
//  Copyright © 2019 Narv. All rights reserved.
//

import SpriteKit
import AVFoundation

class MenuScene: SKScene {
    
    var sceneManagerDelegate: SceneManagerDelegate?
    var audioPlayer: AVAudioPlayer?
    let buttonYOffset: CGFloat = 10
    let buttonXOffset: CGFloat = 10
    
    init(size: CGSize, sceneManagerDelegate: SceneManagerDelegate) {
        self.sceneManagerDelegate = sceneManagerDelegate
        self.audioPlayer = AVAudioPlayer()
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMove(to view: SKView) {
        initializeSaveData()
        layoutView()
        addMusic()
    }
    
    func addMusic() {
        Audio.changeToMenuMusic()
    }
    
    func initializeSaveData() {
        if GameConstants.enableSave {
            let userDefaultData = UserDefaults.standard.dictionary(forKey: GameConstants.StringConstants.saveDataName)
            if ((userDefaultData?.isEmpty) == nil) {
                let data = GameConstants.SavedData.worldLevelMap
                UserDefaults.standard.set(data, forKey: GameConstants.StringConstants.saveDataName)
            }
        }
    }
    
    func layoutView() {
        let background = SKSpriteNode(imageNamed: GameConstants.StringConstants.mainMenuBg)
        background.size = size
        background.position = CGPoint(x: frame.midX, y: frame.midY)
        addChild(background)
        
        let titleLabel = SKLabelNode(fontNamed: GameConstants.StringConstants.fontName)
        titleLabel.text = GameConstants.StringConstants.titleName
        titleLabel.zPosition = GameConstants.ZPositions.hudFront
        titleLabel.fontSize = GameConstants.FloatConstants.fontSize
        titleLabel.fontColor = UIColor.white
        titleLabel.scale(to: frame.size, width: true, multiplier: 0.8)
        titleLabel.position = CGPoint(x: frame.midX, y: frame.midY*0.75 + titleLabel.frame.size.height)
        addChild(titleLabel)
        
        let startButton = SpriteKitButton(defaultImageName: GameConstants.StringConstants.startButtonImage, action: presentLevelScene, index: 0)
        startButton.zPosition = GameConstants.ZPositions.hudFront
        startButton.position = CGPoint(x: frame.midX, y: frame.minY + startButton.size.height/2 + buttonYOffset*2)
        startButton.scale(to: frame.size, width: true, multiplier: 0.3)
        addChild(startButton)
        
        let credits = SpriteKitButton(defaultImageName: GameConstants.StringConstants.startButtonPlainImage, action: handleButton, index: 0)
        credits.zPosition = GameConstants.ZPositions.hudFront
        credits.position = CGPoint(x: frame.maxX - credits.size.width/2 - buttonXOffset, y: frame.minY + credits.size.height/2 + buttonYOffset)
        credits.setText(text: "Credits")
        addChild(credits)
    }
    
    func handleButton(idx: Int) {
        sceneManagerDelegate?.presentCreditsScene()
    }
    
    func presentLevelScene(index: Int) {
        if let _ = UserDefaults.standard.value(forKey: GameConstants.Tutorial.introSaveKey) {
            sceneManagerDelegate?.presentWorldScene()
        } else {
            if GameConstants.enableSave {
                UserDefaults.standard.setValue(true, forKey: GameConstants.Tutorial.introSaveKey)
            }
            sceneManagerDelegate?.presentIntroScene()
        }
    }
}
