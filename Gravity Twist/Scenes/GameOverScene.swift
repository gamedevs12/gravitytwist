//
//  GameOverScene.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 5/20/21.
//  Copyright © 2021 Narv. All rights reserved.
//

import SpriteKit

class GameOverScene: SKScene {
    
    var world: Int
    var level: Int
    var player = Player()
    var state = GameConstants.PlayerState.IdleRight
    var sceneManagerDelegate: SceneManagerDelegate?
    var popUpUI: PopupUI?
    
    init(size: CGSize, world: Int, level: Int, sceneManagerDelegate: SceneManagerDelegate, state: GameConstants.PlayerState = .IdleRight) {
        self.state = state
        self.world = world
        self.level = level
        self.sceneManagerDelegate = sceneManagerDelegate
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMove(to view: SKView) {
        // Focus with background
        addBackground()
        // Add Player
        addPlayer()
        // Animate Player
        animatePlayer()
    }
    
    func addBackground() {
        let bg = SKSpriteNode(imageNamed: "GameOver-Bg")
        bg.position = CGPoint(x: frame.midX, y: frame.midY)
        bg.zPosition = GameConstants.ZPositions.background
        addChild(bg)
    }
    
    func addPlayer() {
        player = Player(imageNamed: GameConstants.StringConstants.playerName)
        player.scale(to: frame.size, width: false, multiplier: 0.5)
        player.name = GameConstants.StringConstants.playerName
        //PhysicsHelper.addPhysics(player)
        player.position = CGPoint(x: frame.midX, y: frame.midY)
        player.zPosition = GameConstants.ZPositions.player
        player.extractTextures(GameConstants.StringConstants.evelynSpriteSheetName)
        player.state = .IdleRight
        //player.turnGravityOn(value: false)
        addChild(player)
        self.run(SKAction.playSoundFileNamed(GameConstants.Sound.bossEnd, waitForCompletion: false))
    }
    
    func animatePlayer() {
        player.animateOnce(for: state, with: 0.2)
    }
    
    func displayOptions() {
        if popUpUI == nil {
            popUpUI = PopupUI(text: "Game Over", imageName: "OptionMenu-Background", size: frame.size)
        }
        if popUpUI?.parent != nil {
            return
        }
        var buttons = [SpriteKitButton]()
        var btn = SpriteKitButton(defaultImageName: "OptionMenu_Button",action: handleButton, index: 0)
        btn.setText(text: "Retry")
        buttons.append(btn)
        btn = SpriteKitButton(defaultImageName: "OptionMenu_Button", action: handleButton, index: 2)
        btn.setText(text: "Level Select")
        buttons.append(btn)
        btn = SpriteKitButton(defaultImageName: "OptionMenu_Button", action: handleButton, index: 3)
        btn.setText(text: "World Select")
        buttons.append(btn)
        popUpUI?.addButtons(SKButtonMap: buttons)
        popUpUI?.position = CGPoint(x: frame.midX, y: frame.midY)
        popUpUI?.zPosition = GameConstants.ZPositions.hudFront
        addChild(popUpUI!)
    }
    
    func handleButton(idx: Int) {
        switch idx {
        case 0:
            sceneManagerDelegate?.presentGameScene(for: world, in: level)
        case 2:
            sceneManagerDelegate?.presentLevelScene(for: level, in: world)
        case 3:
            sceneManagerDelegate?.presentWorldScene()
        default:
            debugPrint("invalid button pressed")
        }
    }
}
