//
//  LevelSelectScene.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 2/26/19.
//  Copyright © 2019 Narv. All rights reserved.
//

import SpriteKit

class LevelSelectScene: SKScene {
    
    var sceneDelegate: SceneManagerDelegate?
    var world: Int
    var idx: Int
    var totalSubscenes: Int
    var LevelSubs: [LevelSceneSub]
    
    init(size: CGSize, world: Int, idx: Int, delegate: SceneManagerDelegate) {
        self.sceneDelegate = delegate
        self.world = world
        self.idx = idx
        self.totalSubscenes = 0
        LevelSubs = [LevelSceneSub]()
        super.init(size: size)
        
        addBackground()
        loadMenu()
        loadSceneSubs()
        loadTopButtons()
        Audio.changeToMenuMusic()
    }
    
    func addBackground() {
        let background = SKSpriteNode(imageNamed: "LevelMenu-Home-Background")
        background.position = CGPoint(x: frame.midX, y: frame.midY)
        background.scale(to: frame.size)
        background.zPosition = GameConstants.ZPositions.background
        background.alpha = 0.3
        self.addChild(background)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func loadMenu() {
        let levelCount = GameConstants.WorldLevelMapping.map[world]!
         
        totalSubscenes = levelCount/GameConstants.LevelConstants.levelSubcount + ((levelCount%GameConstants.LevelConstants.levelSubcount) == 0 ? 0 : 1)
        debugPrint ("Total levels: \(levelCount)")
        debugPrint ("Total SubScene Levels: \(totalSubscenes)")
        for idx in 0...totalSubscenes {
            LevelSubs.append(LevelSceneSub(size: frame.size, index: idx, levels: levelCount, delegate: sceneDelegate!))
        }
        
        for l in LevelSubs {
            addChild(l)
        }
    }
    
    func loadSceneSubs() {
        LevelSubs[idx].loadLayout(buttonAction: goToLevel)
    }
    
    func loadTopButtons() {
        let worldButton = SpriteKitButton(defaultImageName: "WorldOption", action: goBack, index: -1)
        worldButton.position = CGPoint(x: frame.maxX*(4/11), y: frame.maxY*(7/8))
        worldButton.scale(to: frame.size, width: true, multiplier: 0.08)
        worldButton.zPosition = GameConstants.ZPositions.hudFront
        addChild(worldButton)
        let backButton = SpriteKitButton(defaultImageName: "BackButton", action: traverseSubs, index: 0)
        backButton.position = CGPoint(x: frame.maxX*(5.5/11), y: frame.maxY*(7/8))
        backButton.zPosition = GameConstants.ZPositions.hudFront
        addChild(backButton)
        let forwardButton = SpriteKitButton(defaultImageName: "ForwardButton", action: traverseSubs, index: 1)
        forwardButton.position = CGPoint(x: frame.maxX*(6.5/11), y: frame.maxY*(7/8))
        forwardButton.zPosition = GameConstants.ZPositions.hudFront
        addChild(forwardButton)
    }
    
    func goBack(idx: Int) {
        sceneDelegate?.presentWorldScene()
    }
    
    func traverseSubs(i: Int) {
        switch(i){
            case 0:
                if(idx <= 0) {
                    return
                }
                LevelSubs[idx].clearLayout()
                idx = idx - 1
            case 1:
                if(idx >= totalSubscenes - 1) {
                    return
                }
                LevelSubs[idx].clearLayout()
                idx = idx + 1
            default:
                debugPrint("Button Unsupported")
        }
        LevelSubs[idx].loadLayout(buttonAction: goToLevel)
    }
    
    func goToLevel(idx: Int) {
        debugPrint("Level button clicked for \(world) in \(idx+1)")
        sceneDelegate?.presentGameScene(for: world, in: idx+1)
    }
}
