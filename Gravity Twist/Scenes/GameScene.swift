//
//  GameScene.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 1/30/18.
//  Copyright © 2018 Narv. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    var mapNode: SKNode!
    var tileMap: SKTileMapNode!
    var sceneManagerDelegate: SceneManagerDelegate?
    var startPoint: CGPoint!
    var popUpUI: PopupUI?
    var startLocation: CGPoint
    var location: CGPoint
    var swipeDistance: CGFloat
    var touched: Bool = false
    var isPlayerVertical: Bool = false
    var isGravityEnabled: Bool = false
    var hasLevelSwitch = false
    var bossRageEffectDone = false
    var isPlayerDying = false
    var itemDialogueDone = false
    var tutorialExecutedOnce = false
    var startBoss = true
    var isBossLevel = false
    var finishedAllLevels = false
    var pausedBeforeBackground = false
    
    // constant variables
    let accelOnThreshold: Double = 0.7
    let accelTiltThreshold: Double = 0.12
    let gravity: Double = -1.0
    let playerSpeedThreshold: CGFloat = 3.0
    let animationDurationFactor = 0.03
    let swipeDetectionThreshold: CGFloat = 1800
    let popUpSize: CGSize = CGSize(width: 256, height: 320)
    let ignoreGestureReadings = 5
    
    // game level variables
    var levelName: String
    var level: Int
    var world: Int
    var player: Player!
    var coinsForNextLevel: Int = 0
    var coinsCollected: Int = 0
    var doorwayReference: SKSpriteNode
    var direction = "down"
    var label: SKLabelNode
    var spriteSheet: SpriteSheet
    var specialItemIndex: Int = 0
    var groundTouchCount = 0
    var gravityFieldList: [SKSpriteNode]
    var fieldDirectionMap : [String: UInt]
    var gravityDirectionEnable : UISwipeGestureRecognizer.Direction = UISwipeGestureRecognizer.Direction.down
    var tapDirectionEnable : GameConstants.Direction = GameConstants.Direction.down
    var bossClass: Boss
    var bossCoins = 0
    var bossCoinsSpawn: [CGPoint]
    var levers : [Int: [SKSpriteNode]]
    var leverHelper: LeverHelper
    var rockInitPosition: CGPoint = CGPoint(x: 0, y: 0)
    var rockDeathThreshold: CGFloat = 50
    var currentGestureReading = 0
    var dialogueClass: DialogueClass
    var tutorialText: [String]
    var tutorialIndex = -1
    var doorAnimationFrames: [SKTexture]
    var doorAnimator = SKSpriteNode()
    var backgroundMusic = SKAudioNode()
    var pauseButton = SpriteKitButton()
    
    init(size: CGSize, world: Int, level: Int, sceneManagerDelegate: SceneManagerDelegate) {
        self.level = level
        self.world = world
        self.sceneManagerDelegate = sceneManagerDelegate
        if world != 99 {
            self.levelName = "Level_\(world)-\(level)"
        } else {
            switch level {
            case 0:
                self.levelName = "FlameTestLevel"
                break
            default:
                self.levelName = "FlameTestLevel"
                break
            }
        }
        self.doorwayReference = SKSpriteNode(color: UIColor.clear, size: CGSize(width: 0, height: 0))
        self.label = SKLabelNode(text: self.levelName)
        self.popUpUI = PopupUI(text: "Options", imageName: "OptionMenu-Background", size: size)
        self.location = CGPoint(x: 0, y: 0)
        self.startLocation = CGPoint(x: 0, y: 0)
        self.swipeDistance = 0
        self.spriteSheet = SpriteSheet(world: world, level: level)
        self.gravityFieldList = [SKSpriteNode]()
        self.fieldDirectionMap = [String: UInt]()
        self.bossClass = Boss()
        self.bossCoinsSpawn = [CGPoint]()
        self.levers = [Int: [SKSpriteNode]]()
        self.leverHelper = LeverHelper(world, level: level)
        self.dialogueClass = DialogueClass(world: world, level: level)
        self.tutorialText = GameConstants.Tutorial.levelTextMap[GameConstants.Point(x: world, y: level)] ?? [String]()
        self.doorAnimationFrames = AnimationHelper.getDoorAnimation()
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // Called immediately once scene is presented by a view
    override func didMove(to view: SKView) {
        // Init setup of world
        physicsWorld.contactDelegate = self
        physicsWorld.gravity = CGVector(dx: 0.0, dy: gravity)
        
        physicsBody = nil
        initDialogue()
        spriteSheet.splitSprites()
        addBackground()
        load()
        physicsBody = SKPhysicsBody(edgeLoopFrom: CGPath(rect: CGRect(x: frame.minX, y: frame.minY, width: frame.maxX, height: frame.maxY), transform: nil))
        physicsBody!.categoryBitMask = GameConstants.PhysicsCategories.frameCategory
        physicsBody!.contactTestBitMask = GameConstants.PhysicsCategories.playerCategory
        
        doorwayReference.zPosition = GameConstants.ZPositions.platform
        doorAnimator = SKSpriteNode(imageNamed: GameConstants.TextureConstants.doorTextureName)
        doorAnimator.position = doorwayReference.position
        doorAnimator.zPosition = GameConstants.ZPositions.abovePlayer
        doorAnimator.size = doorwayReference.size
        doorAnimator.zRotation = doorwayReference.zRotation
        doorAnimator.alpha = 0
        doorwayReference.parent?.addChild(doorAnimator)
        if isBossLevel {
            Audio.changeToBossMusic()
        }
        else {
            Audio.changeToGamePlayMusic()
        }
        addPauseButton()
        //sleep(1) // to help action run
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc func appMovedToBackground() {
        debugPrint("Going to background, isPaused:\(isPaused), pausedBeforeBackground: \(pausedBeforeBackground)")
        if self.isPaused {
            pausedBeforeBackground = true
        }
    }
    
    @objc func appMovedToForeground() {
        debugPrint("Coming to Foreground, isPaused:\(isPaused), pausedBeforeBackground: \(pausedBeforeBackground)")
        if pausedBeforeBackground {
            longPressDetection(gesture: UITapGestureRecognizer())
        }
    }
    
    func addPauseButton() {
        pauseButton = SpriteKitButton(defaultImageName: GameConstants.StringConstants.pauseButtonName, action: pauseButtonAction, index: 0)
        pauseButton.alpha = 0.70
        pauseButton.scale(to: frame.size, width: true, multiplier: 0.07)
        pauseButton.position = CGPoint(x: frame.maxX-pauseButton.size.width, y: frame.maxY-pauseButton.size.height)
        pauseButton.defaultButton.zPosition = GameConstants.ZPositions.hudFront
        addChild(pauseButton)
    }
    
    func pauseButtonAction(idx: Int) {
        longPressDetection(gesture: UITapGestureRecognizer())
    }
    
    func addTutorialText(text: String) {
        if let mod = self.childNode(withName: GameConstants.Tutorial.moduleName) {
            mod.removeFromParent()
        }
        let backdrop = SKSpriteNode(imageNamed: GameConstants.Tutorial.frameName)
        backdrop.name = GameConstants.Tutorial.moduleName
        backdrop.position = CGPoint(x: frame.midX, y: frame.midY)
        let label = SKLabelNode(fontNamed: GameConstants.StringConstants.fontName)
        label.position = CGPoint(x: label.position.x, y: label.position.y - 5)
        label.text = text
        label.fontColor = UIColor.brown
        label.fontSize = 20
        label.zPosition = GameConstants.ZPositions.hudBack
        backdrop.addChild(label)
        backdrop.zPosition = GameConstants.ZPositions.hudBack
        addChild(backdrop)
    }
    
    func initDialogue() {
        dialogueClass.dialogueSprite.zPosition = GameConstants.ZPositions.hudFront
        dialogueClass.dialogueSprite.position = CGPoint(x: frame.width/2, y: frame.height/2)
        dialogueClass.setup()
        addChild(dialogueClass.dialogueSprite)
        if dialogueClass.isVisible {
            addTutorialText(text: GameConstants.StringConstants.tapText)
        }
        if let currentWorldMap = GameConstants.BossConstants.worldBossMap[world] {
            if currentWorldMap.keys.contains(level) {
                startBoss = false
            }
        }
    }
    
    func addBackground() {
        var bg = SKSpriteNode(imageNamed: "Home-Background-Dark-16-9")
        if world == 0 && level == 75 {
            bg = SKSpriteNode(imageNamed: "Home-Bg-Dark-Broken-16-9")
        }
        bg.scale(to: frame.size)
        bg.position = CGPoint(x: frame.midX, y: frame.midY)
        bg.zPosition = GameConstants.ZPositions.background
        addChild(bg)
    }
    
    func load() {
        if let levelScene = SKScene(fileNamed: levelName) {
            if let levelNode = levelScene.childNode(withName: "BW Tiles")
              as? SKTileMapNode {
                levelNode.removeFromParent()
                tileMap = levelNode
                tileMap.zPosition = GameConstants.ZPositions.platform
                addChild(tileMap)
                loadTileMaps()
                let point = GameConstants.Point(x: world, y: level)
                if !hasLevelSwitch || GameConstants.LevelConstants.levelShiftInitMap.contains(point)
                {
                    leverHelper.startLevelShift(tileMap)
                }
            } else {
                sceneManagerDelegate?.presentWorldScene()
            }
        }
    }
    
    func manageCoinTextures(_ sprite: SKSpriteNode) {
        if let specialItemCount = spriteSheet.specialItemLevelMap[level]?.count, specialItemIndex < specialItemCount {
            let worldLevelPoint = GameConstants.Point(x: world, y: level)
            // check if random texture is needed
            if GameConstants.TextureConstants.levelItemRandomize.contains(worldLevelPoint) {
                let idx = Int.random(in: 0..<specialItemCount)
                if let tex = spriteSheet.specialItemLevelMap[level]?[idx] {
                    ObjectHelper.textureReplace(sprite, texture: tex)
                } else {
                    ObjectHelper.addAction(sprite, atlas: spriteSheet.coinAtlas)
                }
            } else {
                if let tex = spriteSheet.specialItemLevelMap[level]?[specialItemIndex] {
                    if level == 5 {
                        ObjectHelper.addAction(sprite, frames: spriteSheet.animatedObjects[GameConstants.ObjectTypes.note]!)
                    } else {
                        ObjectHelper.textureReplace(sprite, texture: tex)
                    }
                    specialItemIndex = specialItemIndex + 1
                } else {
                    ObjectHelper.addAction(sprite, atlas: spriteSheet.coinAtlas)
                }
            }
        } else {
            ObjectHelper.addAction(sprite, atlas: spriteSheet.coinAtlas)
        }
    }
    
    func updateFieldAction(_ sprite: SKSpriteNode, for change: String)
    {
        let typeList = change.split(separator: ",")
        let atlas1 = spriteSheet.gravityFields[Int(typeList[0])!]
        let atlas2 = spriteSheet.gravityFields[Int(typeList[1])!]
        let frames1 = AnimationHelper.loadTextures(from: atlas1!, withName: "field")
        let frames2 = AnimationHelper.loadTextures(from: atlas2!, withName: "field")
        let fieldAction1 = SKAction.run {
            self.fieldDirectionMap[change] = UInt(typeList[0])!
        }
        let fieldAction2 = SKAction.run {
            self.fieldDirectionMap[change] = UInt(typeList[1])!
        }
        sprite.run(SKAction.repeatForever(
                SKAction.sequence(
                    [fieldAction1, SKAction.animate(with: frames1, timePerFrame: GameConstants.TextureConstants.defaultAnimationSpeed), fieldAction2,
                     SKAction.animate(with: frames2, timePerFrame: GameConstants.TextureConstants.defaultAnimationSpeed)])))
    }
    
    func manageBossTextures(_ sprite: SKSpriteNode) {
        isBossLevel = true
        bossCoins = sprite.userData?["coins"] as! Int
        let sheetName = sprite.userData?["sheetName"] as! String
        let type = GameConstants.BossTypes(rawValue: sprite.userData?["type"] as! Int)!
        bossClass.setCommonBossAttr(sprite: sprite, world: world, level: level, sheetName: sheetName, bossType: type, parentFrame: frame)
        bossClass.Setup()
        if startBoss {
            runBoss()
        }
    }
    
    func runBoss() {
        switch bossClass.bossType {
            case .RobotArm:
                bossClass.currentState = .moveright
                bossClass.Movement()
            case .Suction:
                bossClass.currentState = .appear
                bossClass.Movement()
                break
        }
    }
    
    fileprivate func setupNode(_ sprite: SKSpriteNode) {
        switch(sprite.name!) {
        case GameConstants.StringConstants.coinName:
            manageCoinTextures(sprite)
            coinsForNextLevel += 1
            bossCoinsSpawn.append(sprite.position)
        case GameConstants.StringConstants.doorName:
            doorwayReference = sprite
            doorwayReference.alpha = 0.0
        case GameConstants.StringConstants.enemyName:
           let actionLeft = enemyMovement(type: .FlyLeft, height: sprite.size.height)
           let actionRight = enemyMovement(type: .FlyRight, height: sprite.size.height)
            sprite.run(SKAction.repeatForever(SKAction.sequence([actionLeft, actionLeft, actionRight, actionRight])))
        case GameConstants.StringConstants.startPoint:
            // scale position of player to frame size
            startPoint = CGPoint(x: (self.frame.maxX/tileMap.mapSize.width)*(sprite.position.x), y: (self.frame.maxY/tileMap.mapSize.height)*(sprite.position.y))
            if let tileInfo = sprite.userData?.value(forKey: "direction") {
                direction = tileInfo as! String
            }
        case GameConstants.StringConstants.gravityField:
            let type = sprite.userData?["type"] as! Int
            ObjectHelper.addAction(sprite, atlas: spriteSheet.gravityFields[type]!)
            sprite.isUserInteractionEnabled = false
            if let _ = sprite.userData?["change"] {
                let change = sprite.userData?["change"] as! String
                updateFieldAction(sprite, for: change)
            }
        case GameConstants.StringConstants.warningName:
            bossClass.setWarningSprite(sprite)
        case GameConstants.StringConstants.bossRangeName:
            bossClass.setBossRange(sprite)
        case GameConstants.StringConstants.bossRageName:
            bossClass.setBossRageSprite(sprite)
        case GameConstants.StringConstants.bossName:
            manageBossTextures(sprite)
        case GameConstants.StringConstants.leverName:
            hasLevelSwitch = true
            if sprite.children.count > 0 {
                let group = Int(sprite.children[0].name!)!
                if levers[group] != nil {
                    levers[group]?.append(sprite)
                } else {
                    levers[group] = [sprite]
                }
            }
        case GameConstants.StringConstants.rockGroupName:
            if sprite.children.count > 0 {
                for child in sprite.children {
                    setupNode(child as! SKSpriteNode)
                    ObjectHelper.handleChild(child as! SKSpriteNode, CGVector(dx: 0 , dy: 0))
                }
            }
        case GameConstants.StringConstants.rockName:
            sprite.userData = NSMutableDictionary()
            let actualPosition = CGPoint(x: sprite.parent!.position.x, y: sprite.parent!.position.y)
            sprite.userData?.setValue(actualPosition.x + sprite.position.x, forKey: "xPos")
            sprite.userData?.setValue(actualPosition.y + sprite.position.y, forKey: "yPos")
            sprite.userData?.setValue(sprite.xScale, forKey: "xScale")
            sprite.userData?.setValue(sprite.yScale, forKey: "yScale")
            sprite.zPosition = GameConstants.ZPositions.platform
        case GameConstants.StringConstants.rockGeneratorName:
            sprite.zPosition = GameConstants.ZPositions.abovePlayer
        case GameConstants.StringConstants.movingPlatformName:
            if sprite.children.count > 0 {
                for child in sprite.children {
                    setupNode(child as! SKSpriteNode)
                    ObjectHelper.handleChild(child as! SKSpriteNode, CGVector(dx: 0 , dy: 0))
                }
            }
            var x : CGFloat = 0
            var y : CGFloat = 0
            if let dx = sprite.userData?["x"] as? CGFloat {
                x = dx
            }
            if let dy = sprite.userData?["y"] as? CGFloat {
                y = dy
            }
            if x == 0 && y == 0 {
                return
            }
            var duration = 2.0
            if let d = sprite.userData?["duration"] as? Double {
                duration = d
            }
            sprite.run(SKAction.repeatForever(SKAction.sequence([
                SKAction.moveBy(x: x, y: y, duration: duration),
                SKAction.moveBy(x: -x, y: -y, duration: duration)
            ])))
        case GameConstants.StringConstants.bossStartName, GameConstants.StringConstants.bossEndName:
            bossClass.addPosition(sprite.position)
        default:
            break
        }
    }
    
    func loadTileMaps() {
        tileMap.uniformScale(to: frame.size, multiplier: 1.0)
        PhysicsHelper.addGroundPhysics(tileMapNode: tileMap, with: "ground")
        for child in tileMap.children {
            if let sprite = child as? SKSpriteNode, sprite.name != nil, sprite.name != "horizontal", sprite.name != "vertical" {
                setupNode(sprite)
                ObjectHelper.handleChild(sprite, CGVector(dx: tileMap.mapSize.width , dy: tileMap.mapSize.height))
            }
        }
        addPlayer()
        initPhysicsWorld()
    }
    
    func addPlayer() {
        player = Player(imageNamed: GameConstants.StringConstants.playerName)
        player.scale(to: frame.size, width: false, multiplier: 0.25)
        player.name = GameConstants.StringConstants.playerName
        PhysicsHelper.addPhysics(player)
        if let playerPosition = startPoint {
            player.position = playerPosition
        } else {
            player.position = CGPoint(x: frame.midX/8.0, y: frame.midY/2.0)
        }
        player.zPosition = GameConstants.ZPositions.player
        player.anchorPoint = CGPoint(x: 0.6, y: 0.5)
        player.extractTextures(GameConstants.StringConstants.evelynSpriteSheetName)
        player.state = .IdleRight
        player.turnGravityOn(value: true)
        addChild(player)
    }
    
    func initPhysicsWorld() {
        switch(direction) {
        case "left":
            physicsWorld.gravity = CGVector(dx: gravity, dy: 0.0)
            player.zRotation = -(CGFloat.pi/2)
        case "right":
            physicsWorld.gravity = CGVector(dx: -gravity, dy: 0.0)
            player.zRotation = CGFloat.pi/2
        case "up":
            physicsWorld.gravity = CGVector(dx: 0.0, dy: -gravity)
            player.zRotation = -(CGFloat.pi)
        case "down":
            physicsWorld.gravity = CGVector(dx: 0.0, dy: gravity)
            player.zRotation = 0.0
        default:
            break
        }
    }
    
    func enemyMovement(type: GameConstants.enemyMoveType, height: CGFloat) -> SKAction {
        var action: SKAction
        let rand = Double(arc4random_uniform(5))/10.0
        switch(type) {
        case .UpDown:
            let moveUp = SKAction.move(by: CGVector(dx: 0.0, dy: -height*3), duration: rand)
            let moveDown = SKAction.move(by: CGVector(dx: 0.0, dy: height*3), duration: rand)
            action = SKAction.sequence([moveUp, moveDown])
            break
        case .Curve:
            action = SKAction.applyAngularImpulse(0.02, duration: rand)
            break
        case .FlyRight:
            action = SKAction.customAction(withDuration: 2.0, actionBlock: { (node, curtime) in
                let val = 20 * sin(2 * Double.pi * (Double(curtime) / rand))
                node.position = CGPoint(x: node.position.x + 1, y: node.position.y + CGFloat(val))
            })
        case .FlyLeft:
            action = SKAction.customAction(withDuration: 2.0, actionBlock: { (node, curtime) in
                let val = -20 * sin(2 * Double.pi * (Double(curtime) / rand))
                node.position = CGPoint(x: node.position.x - 1, y: node.position.y + CGFloat(val))
            })
        default:
            action = SKAction.applyAngularImpulse(0.02, duration: rand)
            break
        }
        return action
    }
    
    override func update(_ currentTime: TimeInterval) {
        
        if isPlayerDying || dialogueClass.isVisible {
            return
        }
        if physicsWorld.gravity.dy == 0 {
            isPlayerVertical = true
        } else {
            isPlayerVertical = false
        }
        if(touched) {
            updatePlayerLocation()
        }
    }
    
    func updatePlayerLocation()
    {
        var dx: CGFloat = 0
        var dy: CGFloat = 0
        var playerAnimSpeedFactor: Double
        var playerMoved = false
        if currentGestureReading <= ignoreGestureReadings {
            currentGestureReading += 1
            return
        }
        if isPlayerVertical == false {
            dx = location.x - startLocation.x
            if dx > playerSpeedThreshold {
                dx = playerSpeedThreshold
            } else if dx < -playerSpeedThreshold {
                dx = -playerSpeedThreshold
            }
            playerAnimSpeedFactor = Double(dx)
            if physicsWorld.gravity.dy > 0 {
                playerAnimSpeedFactor *= -1
            }
            if playerAnimSpeedFactor > 0 {
                player.animateOnce(for: .RunRight, with:
                                    playerAnimSpeedFactor*animationDurationFactor)
            } else {
                player.animateOnce(for: .RunLeft, with:
                                    playerAnimSpeedFactor*animationDurationFactor*(-1))
            }
            player.position = CGPoint(x: dx+player.position.x, y: player.position.y)
            playerMoved = true
        } else {
            dy = startLocation.y - location.y
            if dy > playerSpeedThreshold {
                dy = playerSpeedThreshold
            } else if dy < -playerSpeedThreshold {
                dy = -playerSpeedThreshold
            }
            playerAnimSpeedFactor = Double(dy)
            if physicsWorld.gravity.dx < 0 {
                playerAnimSpeedFactor *= -1
            }
            if playerAnimSpeedFactor > 0 {
                player.animateOnce(for: .RunRight, with:
                                    playerAnimSpeedFactor*animationDurationFactor)
            } else {
                player.animateOnce(for: .RunLeft, with:
                                    playerAnimSpeedFactor*animationDurationFactor*(-1))
            }
            player.position = CGPoint(x: player.position.x, y: dy+player.position.y)
            playerMoved = true
        }
        if tutorialExecutedOnce || !playerMoved {
            return
        }
        let levelInfo = GameConstants.Point(x: world, y: level)
        if GameConstants.Tutorial.levelStart.contains(levelInfo) {
            showNextTutorialText()
        }
        tutorialExecutedOnce = true
    }
    
    func showNextTutorialText() {
        let map = GameConstants.Point(x: world, y: level)
        let tutorialTextArray = GameConstants.Tutorial.levelTextMap[map]!
        tutorialIndex += 1
        if tutorialIndex <= tutorialTextArray.count-1 {
            addTutorialText(text: tutorialTextArray[tutorialIndex])
        } else {
            closeTutorialText()
        }
    }
    
    func closeTutorialText() {
        if let mod = self.childNode(withName: GameConstants.Tutorial.moduleName) {
            mod.removeFromParent()
        }
    }
    
    func changeGravity(direction: GameConstants.Direction)
    {
        switch direction {
        case .left: // 2
            physicsWorld.gravity = CGVector(dx: gravity, dy: 0.0)
            player.zRotation = -(CGFloat.pi/2)
        case .right: // 1
            physicsWorld.gravity = CGVector(dx: -gravity, dy: 0.0)
            player.zRotation = CGFloat.pi/2
        case .up: // 4
            physicsWorld.gravity = CGVector(dx: 0.0, dy: -gravity)
            player.zRotation = -(CGFloat.pi)
        case .down: // 3
            physicsWorld.gravity = CGVector(dx: 0.0, dy: gravity)
            player.zRotation = 0.0
        }
    }
    
    @objc func gravityGestureDetection(gesture: UIGestureRecognizer)
    {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            if world == 0 {
                if gravityDirectionEnable != swipeGesture.direction || !isGravityEnabled {
                    return
                }
            }
            print(swipeGesture.direction)
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.left:
                changeGravity(direction: .left)
            case UISwipeGestureRecognizer.Direction.right:
                changeGravity(direction: .right)
            case UISwipeGestureRecognizer.Direction.up:
                changeGravity(direction: .up)
            case UISwipeGestureRecognizer.Direction.down:
                changeGravity(direction: .down)
            default:
                debugPrint("Unknown Swipe Gesture")
            }
        }
    }
    
    func panToSwipeGestureDectector(velocity: CGPoint) -> Bool {
        
        if(velocity.x > swipeDetectionThreshold) {
            let leftSwipe = UISwipeGestureRecognizer()
            leftSwipe.direction = .right
            gravityGestureDetection(gesture: leftSwipe)
            return true
        } else if(velocity.x < -swipeDetectionThreshold) {
            let rightSwipe = UISwipeGestureRecognizer()
            rightSwipe.direction = .left
            gravityGestureDetection(gesture: rightSwipe)
            return true
        }
        if(velocity.y > swipeDetectionThreshold) {
            let downSwipe = UISwipeGestureRecognizer()
            downSwipe.direction = .down
            gravityGestureDetection(gesture: downSwipe)
            return true
        } else if(velocity.y < -swipeDetectionThreshold) {
            let upSwipe = UISwipeGestureRecognizer()
            upSwipe.direction = .up
            gravityGestureDetection(gesture: upSwipe)
            return true
        }
        return false
    }
    
    @objc func playerGestureDetection(gesture: UIPanGestureRecognizer)
    {
        if panToSwipeGestureDectector(velocity: gesture.velocity(in: self.view)) {
            touched = false // fixes phantom movement with gravity
            return
        }
        
        if gesture.state == .began {
            startLocation = gesture.location(in: self.view)
            touched = true
            return
        }
        
        if gesture.state == .ended {
            let map = GameConstants.Point(x: world, y: level)
            if GameConstants.Tutorial.levelStart.contains(map) && !dialogueClass.isVisible {
                tutorialIndex = -1
                tutorialExecutedOnce = false
                showNextTutorialText()
            }
            touched = false
            player.isAnimating = false
            if !isPlayerDying {
                player.state = .IdleRight
            }
            currentGestureReading = 0
        }
        location = gesture.location(in: self.view)
    }
    
    @objc func longPressDetection(gesture: UIGestureRecognizer)
    {
        var useTutorialButtons = false
        let map = GameConstants.Point(x: world, y: level)
        if GameConstants.Tutorial.longPress.contains(map) {
            closeTutorialText()
            useTutorialButtons = true
        }
        if popUpUI == nil {
            popUpUI = PopupUI(text: "Options", imageName: "OptionMenu-Background", size: frame.size)
        }
        if popUpUI?.parent != nil || dialogueClass.isVisible {
            return
        }
        var buttons = [SpriteKitButton]()
        let restartBtn = SpriteKitButton(defaultImageName: GameConstants.StringConstants.optionButtonName,action: handleButton, index: 0)
        restartBtn.setText(text: "Restart")
        let continueBtn = SpriteKitButton(defaultImageName: GameConstants.StringConstants.optionButtonName, action: handleButton, index: 1)
        continueBtn.setText(text: "Continue")
        let levelSelectBtn = SpriteKitButton(defaultImageName: GameConstants.StringConstants.optionButtonName, action: handleButton, index: 2)
        levelSelectBtn.setText(text: "Select Level")
        let worldSelectBtn = SpriteKitButton(defaultImageName: GameConstants.StringConstants.optionButtonName, action: handleButton, index: 3)
        worldSelectBtn.setText(text: "Select World")
        if useTutorialButtons {
            restartBtn.alpha = 0.5
            restartBtn.enabled(value: false)
            levelSelectBtn.alpha = 0.5
            levelSelectBtn.enabled(value: false)
            worldSelectBtn.alpha = 0.5
            worldSelectBtn.enabled(value: false)
        }
        buttons.append(restartBtn)
        buttons.append(continueBtn)
        buttons.append(levelSelectBtn)
        buttons.append(worldSelectBtn)
        popUpUI?.addButtons(SKButtonMap: buttons)
        popUpUI?.position = CGPoint(x: frame.midX, y: frame.midY)
        popUpUI?.zPosition = GameConstants.ZPositions.hudFront
        addChild(popUpUI!)
        isPaused = true
    }
    
    @objc func tapDetected(gesture: UITapGestureRecognizer)
    {
        if dialogueClass.isVisible {
            addTutorialText(text: GameConstants.StringConstants.tapText)
            dialogueClass.nextDialogue(isItemDialogue: false)
            if !dialogueClass.isVisible {
                closeTutorialText()
                if !startBoss {
                    runBoss()
                }
                let worldMap = GameConstants.Point(x: world, y: level)
                if GameConstants.Tutorial.longPress.contains(worldMap) || GameConstants.Tutorial.levelStart.contains(worldMap) {
                    if GameConstants.Tutorial.levelStart.contains(worldMap) {
                        tutorialIndex = -1
                        tutorialExecutedOnce = false
                    }
                    showNextTutorialText()
                }
            }
        }
        if isGravityEnabled {
            switch tapDirectionEnable {
            case GameConstants.Direction.left:
                changeGravity(direction: .left)
            case GameConstants.Direction.right:
                changeGravity(direction: .right)
            case GameConstants.Direction.up:
                changeGravity(direction: .up)
            case GameConstants.Direction.down:
                changeGravity(direction: .down)
            }
        }
    }
    
    func handleButton(idx: Int) {
        switch idx {
        case 0:
            sceneManagerDelegate?.presentGameScene(for: world, in: level)
        case 1:
            isPaused = false
            popUpUI?.removeFromParent()
            popUpUI = nil
        case 2:
            sceneManagerDelegate?.presentLevelScene(for: level, in: world)
        case 3:
            sceneManagerDelegate?.presentWorldScene()
        default:
            debugPrint("invalid button pressed")
        }
    }
    
    func playerPostAnimation() {
        player.turnGravityOn(value: false)
        player.physicsBody = nil
        sceneManagerDelegate?.presentGameScene(for: world, in: level)
    }
    
    func die(reason: Int)
    {
        Audio.stopMusic()
        isPlayerDying = true
        switch reason {
        case 0: // out of bounds
            player.physicsBody?.applyForce(CGVector(dx: 0, dy: 5.0))
            player.turnGravityOn(value: false)
            player.physicsBody?.isDynamic = false
            leverHelper.stopLevelShift(tileMap)
            player.turnGravityOn(value: false)
            player.physicsBody = nil
            player.animateOnce(for: .DeathFrame, with: 0.25)
            player.run(SKAction.playSoundFileNamed(GameConstants.Sound.gameOver, waitForCompletion: false))
            debugPrint("Death By Frame")
            break
        case 3:
            player.physicsBody?.contactTestBitMask = GameConstants.PhysicsCategories.noCategory
            player.physicsBody = nil
            player.animateOnce(for: .DeathSpike, with: 0.25)
            player.run(SKAction.playSoundFileNamed(GameConstants.Sound.gameOver, waitForCompletion: false))
            debugPrint("Death by Spike")
        case 4:
            player.physicsBody?.contactTestBitMask = GameConstants.PhysicsCategories.noCategory
            player.physicsBody = nil
            player.animateOnce(for: .DeathRock, with: 0.25)
            player.run(SKAction.playSoundFileNamed(GameConstants.Sound.gameOver, waitForCompletion: false))
            debugPrint("Death by Rock")
        case 5:
            //player.animateOnce(for: .DeathRobot, with: 0.25)
            sceneManagerDelegate?.presentGameOverScene(for: level, in: world, with: .DeathRobot)
            debugPrint("Death by Robot")
        case 6:
            //player.animateOnce(for: .DeathSuction, with: 0.2)
            sceneManagerDelegate?.presentGameOverScene(for: level, in: world, with: .DeathSuction)
            debugPrint("Death by Suction")
        case 1: // enemy contact
            fallthrough
        case 2: // Trap contact
            print("Death by Trap/Enemy")
            fallthrough
        default:
            player.physicsBody?.contactTestBitMask = GameConstants.PhysicsCategories.noCategory
            player.physicsBody = nil
            player.animateOnce(for: .DeathFrame, with: 0.3)
        }
    }
    
    func gravitySwipeToDirection(value: UISwipeGestureRecognizer.Direction) -> GameConstants.Direction {
        var rawVal = Int(value.rawValue)
        switch value {
        case UISwipeGestureRecognizer.Direction.down:
            rawVal = 1
        case UISwipeGestureRecognizer.Direction.left:
            rawVal = 3
        case UISwipeGestureRecognizer.Direction.right:
            rawVal = 2
        case UISwipeGestureRecognizer.Direction.up:
            rawVal = 0
        default:
            rawVal = 1
        }
        return GameConstants.Direction.init(rawValue: rawVal)!
    }
}

extension GameScene: SKPhysicsContactDelegate {
    
    func didBegin(_ contact: SKPhysicsContact) {
        let contactBitMask = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask
        switch contactBitMask {
        case GameConstants.PhysicsCategories.playerCategory | GameConstants.PhysicsCategories.groundCategory:
            groundTouchCount += 1
            if groundTouchCount > 1 {
            }
        case GameConstants.PhysicsCategories.playerCategory | GameConstants.PhysicsCategories.collectibleCategory:
            let collectible = contact.bodyA.node?.name == GameConstants.StringConstants.coinName ? contact.bodyA.node as! SKSpriteNode : contact.bodyB.node as! SKSpriteNode
            if (GameConstants.DialogueSequences.levelItemDialogueMap[GameConstants.Point(x: world, y: level)] != nil && !itemDialogueDone) {
                dialogueClass.currentDialogue = -1
                addTutorialText(text: GameConstants.StringConstants.tapText)
                dialogueClass.nextDialogue(isItemDialogue: true)
                itemDialogueDone = true
            }
            self.run(SKAction.playSoundFileNamed(GameConstants.Sound.coin, waitForCompletion: false))
            let coinPos = collectible.position
            collectible.removeFromParent()
            coinsCollected += 1
            if( coinsCollected == coinsForNextLevel ) {
                if bossCoins > 0 {
                    if !bossRageEffectDone {
                        // earthquake effect
                        tileMap.run(SKAction.repeat((SKAction.sequence([SKAction.moveBy(x: 5, y: 2, duration: 0.05), SKAction.moveBy(x: -5, y: -2, duration: 0.05)])), count: 10))
                        bossRageEffectDone = true
                        if !bossClass.isRageMode {
                            if bossClass.isBurstModeDone {
                                bossClass.RageMode()
                            } else {
                                bossClass.BurstRageMode()
                            }
                        }
                    }
                    var rand = Int.random(in: 0...bossCoinsSpawn.count-1)
                    let tex = AnimationHelper.loadTextures(from: spriteSheet.coinAtlas, withName: GameConstants.StringConstants.coinName)
                    let sprite = SKSpriteNode(texture: tex[0], color: UIColor.clear, size: CGSize(width: 150, height: 150))
                    sprite.name = GameConstants.StringConstants.coinName
                    var newPos = bossCoinsSpawn[rand]
                    // make sure we don't spawn at previous coin
                    while newPos == coinPos {
                        bossCoinsSpawn.remove(at: rand)
                        rand = Int.random(in: 0...bossCoinsSpawn.count-1)
                        newPos = bossCoinsSpawn[rand]
                        sprite.position = newPos
                    }
                    sprite.position = bossCoinsSpawn[rand]
                    bossCoinsSpawn.remove(at: rand)
                    setupNode(sprite)
                    ObjectHelper.handleChild(sprite, CGVector(dx: tileMap.mapSize.width , dy: tileMap.mapSize.height))
                    tileMap.addChild(sprite)
                    bossCoins -= 1
                    return
                }
                doorwayReference.alpha = 1.0
                PhysicsHelper.addPhysics(doorwayReference)
            }
        case GameConstants.PhysicsCategories.playerCategory | GameConstants.PhysicsCategories.finishCategory:
            level = level + 1
            if level > GameConstants.WorldLevelMapping.map[world]! {
                world = world + 1
                level = 1
                if world >= GameConstants.WorldConstants.totalSupportedWorlds+1 {
                    finishedAllLevels = true
                }
            }
            if GameConstants.enableSave {
                if let savedData = UserDefaults.standard.dictionary(forKey: GameConstants.StringConstants.saveDataName) as? Dictionary<String, Int> {
                    if world+1 >= savedData["world"]! && level > savedData["level"]! {
                        var data = GameConstants.SavedData.worldLevelMap
                        data["world"] = world + 1 // refered a 0 index but saved as 1 index
                        data["level"] = level
                        UserDefaults.standard.set(data, forKey: GameConstants.StringConstants.saveDataName)
                    }
                }
            }
            player.alpha = 0
            doorwayReference.alpha = 0
            doorAnimator.alpha = 1
            let action = SKAction.animate(with: doorAnimationFrames, timePerFrame: 0.13, resize: true, restore: true)
            doorAnimator.run(SKAction.playSoundFileNamed(GameConstants.Sound.door, waitForCompletion: false))
            doorAnimator.run(action) {
                if self.finishedAllLevels {
                    self.sceneManagerDelegate?.presentAchievementScene()
                } else {
                    //self.sceneManagerDelegate?.presentGameScene(for: self.world, in: self.level)
                    self.sceneManagerDelegate?.presentTransitionScene(nextLevel: self.level, nextWorld: self.world)
                }
            }
        case GameConstants.PhysicsCategories.playerCategory | GameConstants.PhysicsCategories.frameCategory:
            die(reason: 0)
        case GameConstants.PhysicsCategories.playerCategory | GameConstants.PhysicsCategories.enemyCategory:
            let boss = contact.bodyA.node?.name == GameConstants.StringConstants.playerName ? contact.bodyB.node as! SKSpriteNode: contact.bodyA.node as! SKSpriteNode
            var bossType = GameConstants.BossTypes.init(rawValue: 0)
            if boss.name == "Suction-Head" {
                bossType = GameConstants.BossTypes.Suction
            }
            switch(bossType) {
            case .RobotArm:
                die(reason: 5)
            case .Suction:
                die(reason: 6)
            default:
                die(reason: 1)
                debugPrint("Regular Enemy encountered!")
            }
            return
        case GameConstants.PhysicsCategories.playerCategory | GameConstants.PhysicsCategories.trapCategory:
            let trap = contact.bodyA.node?.name == GameConstants.StringConstants.trapName ? contact.bodyA.node as! SKSpriteNode : contact.bodyB.node as! SKSpriteNode
            if( !trap.isHidden ) {
                die(reason: 2)
            }
        case GameConstants.PhysicsCategories.playerCategory | GameConstants.PhysicsCategories.obstacleCategory:
            guard let _ = contact.bodyA.node, let _ = contact.bodyA.node else {
                return
            }
            var obstacle : SKNode
            if contact.bodyA.node?.name == GameConstants.StringConstants.playerName {
                obstacle = contact.bodyB.node!
            } else {
                obstacle = contact.bodyA.node!
            }
            switch(obstacle.name) {
            case GameConstants.StringConstants.rockName:
                if (obstacle.physicsBody?.velocity.dx)! > rockDeathThreshold
                    || (obstacle.physicsBody?.velocity.dx)! < -rockDeathThreshold
                    || (obstacle.physicsBody?.velocity.dy)! > rockDeathThreshold
                    || (obstacle.physicsBody?.velocity.dy)! < -rockDeathThreshold {
                    die(reason: 4)
                }
            case GameConstants.StringConstants.spikesName:
                die(reason: 3)
            default:
                break
            }
        case GameConstants.PhysicsCategories.playerCategory | GameConstants.PhysicsCategories.gravityCategory:
            isGravityEnabled = true
            let field = contact.bodyA.node?.name == GameConstants.StringConstants.gravityField ? contact.bodyA.node as! SKSpriteNode : contact.bodyB.node as! SKSpriteNode
            let dir = field.userData?["type"] as! UInt
            if let data = field.userData?["change"] as? String {
                
                field.removeAllActions()
                
                ObjectHelper.addAction(field, atlas: spriteSheet.gravityFields[Int(fieldDirectionMap[data]!)]!)
                
                gravityDirectionEnable = UISwipeGestureRecognizer.Direction.init(rawValue: GameConstants.TextureConstants.fieldTypeGestureMap[self.fieldDirectionMap[data]!]!)
                tapDirectionEnable = gravitySwipeToDirection(value: gravityDirectionEnable)
            } else {
                gravityDirectionEnable = UISwipeGestureRecognizer.Direction.init(rawValue: GameConstants.TextureConstants.fieldTypeGestureMap[dir]!)
                tapDirectionEnable = gravitySwipeToDirection(value: gravityDirectionEnable)
            }
            if let tutorialIdx = field.userData?["tutorial-idx"] as? Int {
                addTutorialText(text: tutorialText[tutorialIdx])
            }
        case GameConstants.PhysicsCategories.enemyCategory | GameConstants.PhysicsCategories.groundCategory:
            if contact.bodyA.node?.name == GameConstants.StringConstants.bossName || contact.bodyB.node?.name == GameConstants.StringConstants.bossName {
                if bossClass.bossType != .RobotArm {
                    return
                }
                let ground = contact.bodyA.node?.name != GameConstants.StringConstants.bossName ? contact.bodyA.node: contact.bodyB.node
                if(ground?.name! == "vertical") {
                    if bossClass.isRageModeActivated() {
                        bossClass.RageMode()
                        return
                    }
                    bossClass.Movement()
                }
            }
        case GameConstants.PhysicsCategories.playerCategory |  GameConstants.PhysicsCategories.interactCategory:
            let lever = (contact.bodyA.node?.name == GameConstants.StringConstants.leverName ? contact.bodyA.node : contact.bodyB.node)!
            self.run(SKAction.playSoundFileNamed(GameConstants.Sound.lever, waitForCompletion: false))
            if leverHelper.isStopping {
                return
            }
            if lever.children.count > 0 {
                let group: Int = Int(lever.children[0].name!)!
                if let items = levers[group] {
                    leverHelper.currentGroup = group
                    leverHelper.animateAllLevers(items, spriteSheet: spriteSheet)
                }
            } else {
                leverHelper.animateAllLevers([lever as! SKSpriteNode], spriteSheet: spriteSheet)
            }
        case GameConstants.PhysicsCategories.obstacleCategory | GameConstants.PhysicsCategories.miscCategory:
            let rock = (contact.bodyA.node?.name == GameConstants.StringConstants.rockName ? contact.bodyA.node : contact.bodyB.node)
            let rockInitPosition = CGPoint(x: rock!.userData!["xPos"] as! CGFloat, y: rock!.userData!["yPos"] as! CGFloat)
            let rockScale = CGPoint(x: rock!.userData!["xScale"] as! CGFloat, y: rock!.userData!["yScale"] as! CGFloat)
            rock!.removeFromParent()
            let newRock = SKSpriteNode(imageNamed: "Evil-Rock")
            newRock.name = GameConstants.StringConstants.rockName
            newRock.position = rockInitPosition
            newRock.xScale = rockScale.x
            newRock.yScale = rockScale.y
            newRock.zPosition = GameConstants.ZPositions.platform
            newRock.userData = NSMutableDictionary()
            newRock.userData?.setValue(rockInitPosition.x, forKey: "xPos")
            newRock.userData?.setValue(rockInitPosition.y, forKey: "yPos")
            newRock.userData?.setValue(rockScale.x, forKey: "xScale")
            newRock.userData?.setValue(rockScale.y, forKey: "yScale")
            PhysicsHelper.addPhysics(newRock)
            tileMap.addChild(newRock)
            break
        default:
            break
        }
    }
    
    func didEnd(_ contact: SKPhysicsContact) {
        let contactBitMask = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask
        switch contactBitMask {
        case GameConstants.PhysicsCategories.playerCategory | GameConstants.PhysicsCategories.groundCategory:
                groundTouchCount -= 1
        case GameConstants.PhysicsCategories.playerCategory | GameConstants.PhysicsCategories.gravityCategory:
            isGravityEnabled = false
            let field = contact.bodyA.node?.name == GameConstants.StringConstants.gravityField ? contact.bodyA.node as! SKSpriteNode : contact.bodyB.node as! SKSpriteNode
            if let data = field.userData?["change"] as? String {
                updateFieldAction(field, for: data)
            }
            if let _ = field.userData?["tutorial-idx"] as? Int {
                closeTutorialText()
            }
        default:
            break
        }
    }
}
