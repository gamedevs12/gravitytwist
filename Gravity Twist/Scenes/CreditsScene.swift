//
//  CreditsScene.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 6/20/21.
//  Copyright © 2021 Narv. All rights reserved.
//

import SpriteKit

class CreditsScene: SKScene {
    
    var sceneManagerDelegate: SceneManagerDelegate?
    var credits = SKLabelNode(fontNamed: GameConstants.StringConstants.fontName)
    var gesture = UITapGestureRecognizer()
    var currentIndex: Int = 0
    var maxIndex: Int = 0
    
    init(size: CGSize, sceneManagerDelegate: SceneManagerDelegate) {
        self.sceneManagerDelegate = sceneManagerDelegate
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMove(to view: SKView) {
        addTapGestureRecognizer()
        addCreditsLabel()
        addTapToContinueLabel()
        addMusic()
    }
    
    func addMusic() {
        Audio.changeToVictoryMusic()
    }
    
    func addCreditsLabel() {
        credits.text = GameConstants.StringConstants.credits[currentIndex]
        credits.position = CGPoint(x: frame.midX, y: frame.midY)
        credits.fontSize = 28
        credits.preferredMaxLayoutWidth = 500
        credits.lineBreakMode = .byClipping
        credits.numberOfLines = 4
        addChild(credits)
    }
    
    func addTapGestureRecognizer() {
        gesture = UITapGestureRecognizer(target: self, action: #selector(tap))
        self.view?.addGestureRecognizer(gesture)
    }
    
    func addTapToContinueLabel() {
        let tapToContinueLabel = SKLabelNode(fontNamed: GameConstants.StringConstants.fontName)
        tapToContinueLabel.text = GameConstants.StringConstants.tapText
        tapToContinueLabel.zPosition = GameConstants.ZPositions.hudFront
        tapToContinueLabel.position = CGPoint(x: frame.midX, y: frame.minY + 100)
        tapToContinueLabel.fontSize = 20
        tapToContinueLabel.alpha = 1
        tapToContinueLabel.run(
            SKAction.repeatForever(
                SKAction.sequence(
                    [SKAction.fadeAlpha(to: 0, duration: 0.15),
                     SKAction.fadeAlpha(to: 1, duration: 0.15)])))
        addChild(tapToContinueLabel)
    }
    
    @objc func tap() {
        currentIndex += 1
        if currentIndex >= GameConstants.StringConstants.credits.count {
            if let v = self.view {
                v.removeGestureRecognizer(gesture)
            }
            sceneManagerDelegate?.presentMenuScene()
        }
        credits.run(SKAction.fadeOut(withDuration: 0.5)) {
            if self.currentIndex >= GameConstants.StringConstants.credits.count {
                if let v = self.view {
                    v.removeGestureRecognizer(self.gesture)
                }
                self.sceneManagerDelegate?.presentMenuScene()
            }
            self.credits.text = GameConstants.StringConstants.credits[self.currentIndex]
            self.credits.run(SKAction.fadeIn(withDuration: 0.5))
        }
    }
}
