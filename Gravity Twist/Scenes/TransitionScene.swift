//
//  TransitionScene.swift
//  Gravity Phroid
//
//  Created by Abhinav Rathod on 7/25/21.
//  Copyright © 2021 Narv. All rights reserved.
//

import SpriteKit

class TransitionScene: SKScene {
    
    var sceneManagerDelegate: SceneManagerDelegate?
    var tileMap: SKTileMapNode!
    var player: Player!
    
    var startPoint: CGPoint!
    var endPoint: CGPoint!
    var nextLevel: Int
    var nextWorld: Int
    
    init(size: CGSize, nextLevel: Int, nextWorld: Int, sceneManagerDelegate: SceneManagerDelegate) {
        self.sceneManagerDelegate = sceneManagerDelegate
        self.nextLevel = nextLevel
        self.nextWorld = nextWorld
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMove(to view: SKView) {
        addBackground()
        addAppreciation()
        addTilemap()
        addPlayer()
        movePlayer()
    }
    
    func addBackground() {
        let bgTop = SKSpriteNode(imageNamed: "Transition-Background-Top")
        bgTop.scale(to: frame.size, width: true, multiplier: 1.0)
        bgTop.position = CGPoint(x: frame.minX, y: frame.maxY)
        bgTop.anchorPoint = CGPoint(x: 0, y: 1)
        bgTop.zPosition = GameConstants.ZPositions.background
        addChild(bgTop)
        let bgBot = SKSpriteNode(imageNamed: "Transition-Background-Top")
        bgBot.scale(to: frame.size, width: true, multiplier: 1.0)
        bgBot.position = CGPoint(x: frame.minX, y: frame.minY)
        bgBot.anchorPoint = CGPoint(x: 0, y: 0)
        bgBot.zPosition = GameConstants.ZPositions.background
        addChild(bgBot)
    }
    
    func addAppreciation() {
        let label = SKLabelNode(fontNamed: GameConstants.StringConstants.fontName)
        debugPrint(Int(GameConstants.WorldLevelMapping.map[0]!/2))
        if nextLevel < Int(GameConstants.WorldLevelMapping.map[0]!/2) {
            let rand = Int.random(in: 0...GameConstants.Achievement.congratsArray.count-1)
            label.text = GameConstants.Achievement.congratsArray[rand]
        } else {
            let rand = Int.random(in: 0...GameConstants.Achievement.higherCongratsArray.count-1)
            label.text = GameConstants.Achievement.higherCongratsArray[rand]
        }
        label.position = CGPoint(x: frame.midX, y: frame.midY/2 - label.fontSize)
        label.zPosition = GameConstants.ZPositions.hudFront
        label.color = UIColor.black
        //label.run(SKAction.repeatForever(SKAction.sequence([SKAction.fadeAlpha(to: 0, duration: 0.5), SKAction.fadeAlpha(to: 1, duration: 0.5)])))
        addChild(label)
    }
    
    func addTilemap() {
        if let levelScene = SKScene(fileNamed: GameConstants.LevelConstants.transitionSks) {
            if let levelNode = levelScene.childNode(withName: GameConstants.StringConstants.transitionTilesName)
              as? SKTileMapNode {
                levelNode.removeFromParent()
                tileMap = levelNode
                tileMap.zPosition = GameConstants.ZPositions.platform
                addChild(tileMap)
                loadTileMaps()
            }
        }
    }
    
    func loadTileMaps() {
        tileMap.uniformScale(to: frame.size, multiplier: 1.0)
        PhysicsHelper.addGroundPhysics(tileMapNode: tileMap, with: "ground")
        for child in tileMap.children {
            if let sprite = child as? SKSpriteNode, sprite.name != nil, sprite.name != "horizontal", sprite.name != "vertical" {
                setupNode(sprite)
                ObjectHelper.handleChild(sprite, CGVector(dx: tileMap.mapSize.width , dy: tileMap.mapSize.height))
            }
        }
    }
    
    func setupNode(_ sprite: SKSpriteNode) {
        switch (sprite.name) {
        case "Start":
            startPoint = CGPoint(x: (self.frame.maxX/tileMap.mapSize.width)*(sprite.position.x), y: (self.frame.maxY/tileMap.mapSize.height)*(sprite.position.y))
            break
        case "End":
            endPoint = CGPoint(x: (self.frame.maxX/tileMap.mapSize.width)*(sprite.position.x), y: (self.frame.maxY/tileMap.mapSize.height)*(sprite.position.y))
            break
        default:
        break
        }
    }
    
    func addPlayer() {
        player = Player(imageNamed: GameConstants.StringConstants.playerName)
        player.scale(to: frame.size, width: false, multiplier: 0.25)
        player.name = GameConstants.StringConstants.playerName
        PhysicsHelper.addPhysics(player)
        if let playerPosition = startPoint {
            player.position = playerPosition
        } else {
            player.position = CGPoint(x: frame.midX/8.0, y: frame.midY/2.0)
        }
        player.zPosition = GameConstants.ZPositions.player
        player.anchorPoint = CGPoint(x: 0.6, y: 0.5)
        player.extractTextures(GameConstants.StringConstants.evelynSpriteSheetName)
        player.state = .IdleRight
        player.turnGravityOn(value: true)
        addChild(player)
    }
    
    func movePlayer() {
        // traverse from point startPoint to EndPoint
        player.animate(for: .RunRight)
        player.run(SKAction.move(to: endPoint, duration: 5.0)) {
            self.sceneManagerDelegate?.presentGameScene(for: self.nextWorld, in: self.nextLevel)
        }
    }
}

