//
//  Intro_Scene.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 6/12/21.
//  Copyright © 2021 Narv. All rights reserved.
//

import SpriteKit

class IntroScene: SKScene {
    
    var sceneManagerDelegate: SceneManagerDelegate?
    var introTextIdx : Int
    let storyLabel = SKLabelNode(fontNamed: GameConstants.StringConstants.fontName)
    var gesture = UITapGestureRecognizer()
    
    init(size: CGSize, sceneManagerDelegate: SceneManagerDelegate) {
        self.sceneManagerDelegate = sceneManagerDelegate
        self.introTextIdx = 0
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMove(to view: SKView) {
        addTapGestureRecognizer()
        displayIntroText()
        displayTapText()
    }
    
    func addTapGestureRecognizer() {
        gesture = UITapGestureRecognizer(target: self, action: #selector(tap))
        self.view?.addGestureRecognizer(gesture)
    }
    
    func displayIntroText() {
        storyLabel.text = GameConstants.StringConstants.introText[introTextIdx]
        storyLabel.position = CGPoint(x: frame.midX, y: frame.midY)
        storyLabel.fontSize = 28
        storyLabel.preferredMaxLayoutWidth = 500
        storyLabel.lineBreakMode = .byClipping
        storyLabel.numberOfLines = 4
        addChild(storyLabel)
    }
    
    func displayTapText() {
        let tapLabel = SKLabelNode(fontNamed: GameConstants.StringConstants.fontName)
        tapLabel.text = GameConstants.StringConstants.tapText
        tapLabel.position = CGPoint(x: frame.midX, y: frame.minY + 100)
        tapLabel.fontSize = 20
        tapLabel.run(
            SKAction.repeatForever(
                SKAction.sequence(
                        [SKAction.fadeAlpha(to: 0, duration: 0.15),
                         SKAction.fadeAlpha(to: 1, duration: 0.15)])))
        addChild(tapLabel)
    }
    
    @objc func tap() {
        introTextIdx += 1
        if introTextIdx >= GameConstants.StringConstants.introText.count {
            if let v = self.view {
                v.removeGestureRecognizer(gesture)
            }
            sceneManagerDelegate?.presentWorldScene()
        }
        storyLabel.run(SKAction.fadeOut(withDuration: 0.5)) {
            if self.introTextIdx >= GameConstants.StringConstants.introText.count {
                if let v = self.view {
                    v.removeGestureRecognizer(self.gesture)
                }
                self.sceneManagerDelegate?.presentWorldScene()
            }
            self.storyLabel.text = GameConstants.StringConstants.introText[self.introTextIdx]
            self.storyLabel.run(SKAction.fadeIn(withDuration: 0.5))
        }
    }
}
