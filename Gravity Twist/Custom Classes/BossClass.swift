//
//  BossHelper.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 3/23/21.
//  Copyright © 2021 Narv. All rights reserved.
//

import SpriteKit

enum BossState: Int {
    case idle = 0, idlerage, moveleft, moveright, burstleft, burstright, rageleft, rageright,
         appear, disappear, appearrage, disappearrage, extra1, extra2
}

class Boss {
    
    var world: Int
    var level: Int
    var sprite: SKSpriteNode
    var spriteRage: SKSpriteNode
    var sheetName: String
    var bossTexAtlas: [BossState: SKTextureAtlas]
    var bossExtras: [BossState: SKTexture]
    var bossType: GameConstants.BossTypes
    var currentState: BossState
    var isBurstModeDone = false
    var isRageMode = false
    var parentFrame: CGRect
    var bossPosition: [CGPoint]
    var warningRef: SKSpriteNode = SKSpriteNode()
    var warningAnimationCount: Int = 2
    var moveToMarker: SKSpriteNode = SKSpriteNode()
    var suctionYOffset: CGFloat = 300
    var currentBossSprite: SKSpriteNode
    var killRoutine = false
    
    // constants
    var xOffset: CGFloat = 3000
    //var suctionYTraverse: CGFloat = 1000
    var suctionAnimDuration = 2.5
    var robotAnimDuration = 6.5
    let robotRageDuration = 5.0
    let burstDuration = 0.15
    let frameMultipliers: [CGFloat] = [0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]

    init(sprite: SKSpriteNode, world: Int, level: Int, sheetName: String, bossType: GameConstants.BossTypes) {
        self.sprite = sprite
        self.spriteRage = sprite
        self.world = world
        self.level = level
        self.sheetName = sheetName
        self.bossType = bossType
        self.currentState = BossState.idle
        self.bossTexAtlas = [BossState : SKTextureAtlas]()
        self.bossExtras = [BossState: SKTexture]()
        self.parentFrame = CGRect()
        self.bossPosition = [CGPoint]()
        self.currentBossSprite = sprite
    }
    
    init() {
        self.sprite = SKSpriteNode()
        self.spriteRage = SKSpriteNode()
        self.world = 0
        self.level = 1
        self.sheetName = ""
        self.bossType = GameConstants.BossTypes.RobotArm
        self.currentState = BossState.idle
        self.bossTexAtlas = [BossState : SKTextureAtlas]()
        self.bossExtras = [BossState: SKTexture]()
        self.parentFrame = CGRect()
        self.bossPosition = [CGPoint]()
        self.currentBossSprite = sprite
    }
    
    deinit {
        self.bossTexAtlas.removeAll()
        self.bossExtras.removeAll()
    }
    
    func setCommonBossAttr(sprite: SKSpriteNode, world: Int, level: Int, sheetName: String, bossType: GameConstants.BossTypes, parentFrame: CGRect) {
        self.sprite = sprite
        self.spriteRage = sprite
        self.world = world
        self.level = level
        self.sheetName = sheetName
        self.bossType = bossType
        self.parentFrame = parentFrame
        self.currentBossSprite = sprite
    }
    
    func setBossRageSprite(_ sprite: SKSpriteNode) {
        self.spriteRage = sprite
    }
    
    func addPosition(_ point: CGPoint) {
        bossPosition.append(point)
    }
    
    func setWarningSprite(_ sprite: SKSpriteNode) {
        warningRef = sprite
    }
    
    func setBossRange(_ range: SKSpriteNode) {
        moveToMarker = range
    }
    
    func Setup() {
        extractBossTextures()
        if bossType == GameConstants.BossTypes.Suction {
            randomlyPlaceSuctionBoss()
            suctionYOffset = sprite.position.y - moveToMarker.position.y + sprite.yScale*(sprite.childNode(withName: "Suction-Head")?.position.y)!
        }
    }
    
    func randomlyPlaceSuctionBoss() {
        currentBossSprite.position = CGPoint(x: self.bossPosition[1].x * self.frameMultipliers[Int.random(in: 0..<self.frameMultipliers.count)], y: currentBossSprite.position.y)
    }
    
    func extractBossTextures() {
        var bossFrames = [BossState: [CGPoint]]()
        switch bossType {
        case GameConstants.BossTypes.RobotArm:
            bossFrames = GameConstants.BossConstants.robotArmFrames
        case GameConstants.BossTypes.Suction:
            bossFrames = GameConstants.BossConstants.suctionFrames
        }
        
        let cgImg = UIImage(named: sheetName)?.cgImage
        let tileSize = GameConstants.BossConstants.tileSize
        for item in bossFrames {
            let key = item.key
            let start = item.value[0]
            let end = item.value[1]
            let totalTiles = Int(end.x - start.x)
            var frames = [String:UIImage]()
            if totalTiles == 0 {
                let rect = CGRect(x: start.x*tileSize.x, y: start.y*tileSize.y, width: tileSize.x, height: tileSize.y)
                let cgTemp = cgImg?.cropping(to: rect)!
                bossExtras[key] = SKTexture(cgImage: cgTemp!)
                continue
            }
            for i in 0...totalTiles {
                let tileXOffset = CGFloat(i)*tileSize.x
                let rect = CGRect(x: start.x*tileSize.x + tileXOffset, y: start.y*tileSize.y, width: tileSize.x, height: tileSize.y)
                let cgTemp = cgImg?.cropping(to: rect)!
                frames["boss\(key)\(i)"] = UIImage(cgImage: cgTemp!)
            }
            // This can cause crash if pixel art is exported at x5 quality
            // This is due to Atlas memory + other assets maxed in RAM
            bossTexAtlas[key] = SKTextureAtlas(dictionary: frames)
        }
    }

    func robotBossMovement() {
        xOffset *= -1
        if isMovingLeft() {
            currentState = .moveright
        } else {
            currentState = .moveleft
        }
        sprite.removeAllActions()
        let frames = AnimationHelper.loadTextures(from: bossTexAtlas[currentState]!, withName: "boss\(currentState)")
        let animate = SKAction.animate(with: frames, timePerFrame: GameConstants.TextureConstants.defaultAnimationSpeed, resize: false, restore: false)
        let traverse = SKAction.moveTo(x: sprite.position.x + xOffset, duration: robotAnimDuration)
        sprite.run(SKAction.repeatForever(animate))
        sprite.run(traverse)
    }
    
    func isNonRageModeRunning() -> Bool {
        if isRageMode && currentBossSprite.name != GameConstants.StringConstants.bossRageName {
            return true
        }
        return false
    }
    
    func suctionRoutine() {
        if isNonRageModeRunning() {
            return
        }
        let warningAction = getWarningAction()
        let suctionAppearAction = getSuctionAppearAction()
        let suctionIdleAction = getSuctionIdleAction()
        let suctionDisappearAction = getSuctionDisappearAction()
        warningRef.run(warningAction) {
            self.currentState = .appear
            self.currentBossSprite.run(suctionAppearAction) {
                self.setIdleState()
                self.currentBossSprite.childNode(withName: "Suction-Head")?.run(suctionIdleAction) {
                    self.currentState = .disappear
                    self.currentBossSprite.run(suctionDisappearAction) {
                        self.suctionRoutine()
                    }
                }
            }
        }
    }
    
    func getWarningAction() -> SKAction {
        let warning = SKAction.repeat(SKAction.sequence([SKAction.fadeAlpha(to: 1, duration: 0.5), SKAction.fadeAlpha(to: 0, duration: 0.5)]), count: warningAnimationCount)
        warningRef.position = CGPoint(x: currentBossSprite.position.x, y: warningRef.position.y)
        return warning
    }
    
    func setIdleState() {
        if isRageMode {
            currentState = .idlerage
        } else {
            currentState = .idle
        }
    }
    
    func getSuctionAppearAction() -> SKAction {
        let traverse = SKAction.moveBy(x: 0, y: -suctionYOffset, duration: suctionAnimDuration)
        return traverse
    }
    
    func getSuctionIdleAction() -> SKAction {
        var frames = [SKTexture]()
        setIdleState()
        frames = AnimationHelper.loadTextures(from: bossTexAtlas[currentState]!, withName: "boss\(currentState)")
        let animateHead = SKAction.animate(with: frames, timePerFrame: GameConstants.TextureConstants.defaultAnimationSpeed, resize: false, restore: false)
        return SKAction.repeat(animateHead, count: 5)
    }
    
    func getSuctionDisappearAction() -> SKAction {
        let traverseBack = SKAction.moveBy(x: 0, y: suctionYOffset, duration: suctionAnimDuration)
        let moveSuction = SKAction.run {
            self.randomlyPlaceSuctionBoss()
        }
        return SKAction.sequence([traverseBack, moveSuction])
    }
    
    func Movement() {
        switch bossType {
        case .RobotArm:
            robotBossMovement()
        case .Suction:
            suctionRoutine()
        }
    }
    
    func isRageModeActivated() -> Bool {
        return currentState.rawValue > BossState.burstright.rawValue
    }
    
    func isMovingLeft() -> Bool {
        return currentState == BossState.moveleft || currentState == BossState.rageleft
    }
    
    func BurstRageMode() {
        currentBossSprite.removeAllActions()
        var action = SKAction()
        switch bossType {
        case .RobotArm:
            var toBurstState = BossState.burstright
            if currentState == BossState.moveleft {
                toBurstState = BossState.burstleft
            }
            let burstFrames = AnimationHelper.loadTextures(from: bossTexAtlas[toBurstState]!, withName: "boss\(toBurstState)")
            action = SKAction.animate(with: burstFrames, timePerFrame: burstDuration, resize: false, restore: false)
        case .Suction:
            warningRef.removeAllActions()
            currentBossSprite.childNode(withName: "Suction-Head")?.removeAllActions()
            action = SKAction.moveBy(x: 0, y: suctionYOffset, duration: 0.5)
        }
        currentBossSprite.run(action) {
            self.isBurstModeDone = true
            self.RageMode()
        }
    }
    
    func RageMode() {
        switch bossType {
        case .RobotArm:
            var toRageState: BossState
            var xMoveTo: CGFloat
            if currentState == BossState.moveleft || currentState == BossState.rageright {
                toRageState = BossState.rageleft
                xMoveTo = sprite.position.x - xOffset
            } else {
                toRageState = BossState.rageright
                xMoveTo = sprite.position.x + xOffset
            }
            let rageFrames = AnimationHelper.loadTextures(from: bossTexAtlas[toRageState]!, withName: "boss\(toRageState)")
            let rageAction = SKAction.animate(with: rageFrames, timePerFrame: GameConstants.TextureConstants.defaultAnimationSpeed, resize: false, restore: false)
            let traverse = SKAction.moveTo(x: xMoveTo, duration: robotRageDuration)
            
            sprite.run(SKAction.repeatForever(rageAction))
            sprite.run(traverse)
            
            currentState = toRageState
        case .Suction:
            suctionAnimDuration = 0.75
            warningAnimationCount = 1
            isRageMode = true
            currentBossSprite = spriteRage
            suctionRoutine()
        }
    }
}
