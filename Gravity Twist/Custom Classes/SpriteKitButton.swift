//
//  SpriteKitButton.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 3/2/19.
//  Copyright © 2019 Narv. All rights reserved.
//

import SpriteKit

class SpriteKitButton: SKSpriteNode {
    
    var defaultButton = SKSpriteNode()
    var action: ((Int) -> ()?)? = nil
    var index: Int = 0
    var resize: CGSize!
    var enabled: Bool = false
    var defaultText: String = ""
    
    init()
    {
        super.init(texture: nil, color: UIColor.clear, size: CGSize())
    }
    
    init(defaultImageName: String, action: @escaping (Int) -> (), index: Int)
    {
        self.defaultButton = SKSpriteNode(imageNamed: defaultImageName)
        self.action = action
        self.index = index
        self.enabled = true
        self.defaultText = "Default"
        super.init(texture: nil, color: UIColor.clear, size: defaultButton.size)
        isUserInteractionEnabled = enabled
        addChild(defaultButton)
    }
    
    func setText(text: String)
    {
        let label = SKLabelNode(fontNamed: GameConstants.StringConstants.fontName)
        label.text = defaultText
        if text != "" {
            label.text = text
        }
        label.fontSize = 20
        label.verticalAlignmentMode = .center
        label.zPosition = GameConstants.ZPositions.hudFront
        addChild(label)
    }
    
    func startAnimation() {
        self.run(.applyAngularImpulse(0.4, duration: 5) )
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init coder not implemented")
    }
    
    func enabled(value: Bool) {
        self.enabled = value
        self.isUserInteractionEnabled = value
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        defaultButton.alpha = 0.75
        print("SpriteKitButton: touched Began")
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch = touches.first! as UITouch
        let touchPosition: CGPoint = touch.location(in: self)
        
        if (defaultButton.contains(touchPosition)) {
            defaultButton.alpha = 0.75
        } else {
            defaultButton.alpha = 1.0
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch = touches.first! as UITouch
        let touchPosition: CGPoint = touch.location(in: self)
        print("SpriteKitButton: touches Ended")
        
        if(defaultButton.contains(touchPosition)) {
            action!(index)
        }
        
        defaultButton.alpha = 1.0
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        defaultButton.alpha = 1.0
        print("SpriteKitButton: touches Canceled")
    }
}
