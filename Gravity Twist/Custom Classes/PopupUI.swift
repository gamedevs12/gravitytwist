//
//  PopupUI.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 3/5/19.
//  Copyright © 2019 Narv. All rights reserved.
//

import SpriteKit

/*
 Example usage of PopupUI:
 let p = PopupUI(text: "Option Menu", imageName: "MainMenuBg.png")
 var buttons = [SpriteKitButton]()
 buttons.append(SpriteKitButton(defaultImageName: "faceTemp.png", action: handleButton, index: 0))
 buttons.append(SpriteKitButton(defaultImageName: "haloTree.png", action: handleButton, index: 1))
 buttons.append(SpriteKitButton(defaultImageName: "NikFace.png", action: handleButton, index: 2))
 p.addButtons(SKButtonMap: buttons)
 p.position = CGPoint(x: frame.midX, y: frame.midY)
 addChild(p)
 */

class PopupUI: SKSpriteNode {
    
    var popupBg: SKSpriteNode!
    var popupLabel: SKLabelNode!
    var buttonText: [String]
    
    init(text: String, imageName: String, size: CGSize) {
        
        popupBg = SKSpriteNode(imageNamed: imageName)
        popupLabel = SKLabelNode(fontNamed: GameConstants.StringConstants.fontName)
        buttonText = [String]()
        super.init(texture: SKTexture(imageNamed: imageName), color: UIColor.clear, size: CGSize(width: size.width*0.35, height: size.height*0.9))
        popupLabel.position = CGPoint(x: frame.midX, y: frame.maxY*0.5 + popupLabel.frame.size.height)
        popupLabel.text = text
        popupLabel.fontSize = 50
        popupLabel.fontColor = UIColor.black
        popupLabel.zPosition = GameConstants.ZPositions.hudFront
        addChild(popupLabel)
        
    }
    
    func addButtons(SKButtonMap: [SpriteKitButton]) {
        // Can only have 4 buttons at max
        assert(SKButtonMap.count <= 4)
        // values works for 5s onwards
        let spacingFactor: CGFloat = 2.0
        for (index, button) in SKButtonMap.enumerated() {
            button.position = CGPoint(x: frame.midX, y: frame.maxY*0.6 - CGFloat(index+1) * 27 * spacingFactor)
            button.zPosition = GameConstants.ZPositions.hudFront
            addChild(button)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
