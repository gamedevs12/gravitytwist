//
//  LevelSceneSub.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 3/24/19.
//  Copyright © 2019 Narv. All rights reserved.
//

import SpriteKit

class LevelSceneSub: SKSpriteNode {
    
    var sceneDelegate: SceneManagerDelegate
    var index: Int
    var totalLevels: Int
    var levelTiles: [SpriteKitButton]
    
    init(size: CGSize, index: Int, levels: Int, delegate: SceneManagerDelegate) {
        self.sceneDelegate = delegate
        self.index = index
        self.totalLevels = levels
        self.levelTiles = [SpriteKitButton]()
        super.init(texture: nil, color: UIColor.clear, size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func loadLayout(buttonAction: @escaping (_ idx: Int) -> ()) {
        
        var startIndex = 0
        var endIndex = 0
        let totalSubScenes = totalLevels/GameConstants.LevelConstants.levelSubcount + ((totalLevels%GameConstants.LevelConstants.levelSubcount) == 0 ? 0 : 1)
        if index == 0 {
            startIndex = 0
            endIndex = totalLevels > 15 ? 15 : totalLevels
        } else if index == totalSubScenes - 1 {
            startIndex = index*GameConstants.LevelConstants.levelSubcount
            endIndex = startIndex + totalLevels - index*GameConstants.LevelConstants.levelSubcount
        } else {
            startIndex = index*GameConstants.LevelConstants.levelSubcount
            endIndex = startIndex + GameConstants.LevelConstants.levelSubcount
        }
        
        for i in startIndex..<endIndex {
            let button = SpriteKitButton(defaultImageName: "Level\(i+1)Icon", action: buttonAction, index: i)
            if GameConstants.enableSave {
                let levelAccessible = UserDefaults.standard.dictionary(forKey: GameConstants.StringConstants.saveDataName)!["level"] as! Int
                if i+1 > levelAccessible {
                    button.alpha = 0.25
                    button.enabled(value: false)
                }
            }
            levelTiles.append(button)
        }
        
        let yUnits = self.size.height/8
        let xUnits = self.size.width/11
        let initX: CGFloat = 1.5
        let initY: CGFloat = 5.5
        var yScale = initY
        var xScale = initX
        // 5 columns, 3 rows
        for (i,sub) in levelTiles.enumerated() {
            
            let x = CGFloat(xUnits*xScale)
            let y = CGFloat(yUnits*yScale)
            sub.position = CGPoint(x: x, y: y)
            sub.scale(to: frame.size, width: true, multiplier: 0.1)
            sub.zPosition = GameConstants.ZPositions.hudBack
            addChild(sub)
            
            xScale += 2
            
            if (i+1) % 5 == 0 {
                xScale = initX
                yScale -= 2
            }
        }
    }
    
    func clearLayout() {
        for i in levelTiles {
            i.removeFromParent()
        }
        levelTiles.removeAll()
    }
}
