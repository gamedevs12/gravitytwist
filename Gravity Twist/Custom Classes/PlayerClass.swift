//
//  PlayerClass.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 1/31/18.
//  Copyright © 2018 Narv. All rights reserved.
//
// Everything related to player

import SpriteKit
    
enum PlayerDirection {
    case left, right, up, down
}

class Player:SKSpriteNode {
    
    var animFrameMap : [GameConstants.PlayerState: SKTextureAtlas] = [GameConstants.PlayerState: SKTextureAtlas]()
    var playerDirection: PlayerDirection = .right
    var isAnimating = false
    
    var state = GameConstants.PlayerState.IdleRight {
        willSet {
            animate(for: state)
            setDirection(for: state)
        }
    }
    
    func extractTextures(_ sheetName: String) {
        var hasSetTexture = false
        let cgImg = UIImage(named: sheetName)?.cgImage
        let tileSize = GameConstants.TextureConstants.playerTileSize
        for item in GameConstants.TextureConstants.playerAnimMap {
            let key = item.key
            let start = item.value[0]
            let end = item.value[1]
            let totalTiles = Int(end.x - start.x)
            var frames = [String:UIImage]()
            for i in 0...totalTiles {
                let tileXOffset = CGFloat(i)*tileSize.x
                let rect = CGRect(x: start.x*tileSize.x + tileXOffset, y: start.y*tileSize.y, width: tileSize.x, height: tileSize.y)
                let cgTemp = cgImg?.cropping(to: rect)!
                frames["\(key)\(i)"] = UIImage(cgImage: cgTemp!)
                if !hasSetTexture && key == .IdleRight {
                    self.texture = SKTexture(cgImage: cgTemp!)
                    hasSetTexture = true
                }
            }
            animFrameMap[key] = SKTextureAtlas(dictionary: frames)
        }
    }
    
    func animate(for state: GameConstants.PlayerState) {
        removeAllActions()
        let frames = AnimationHelper.loadTextures(from: animFrameMap[state]!, withName: "\(state)")
        self.run(SKAction.repeatForever(SKAction.animate(with: frames, timePerFrame: 0.1, resize: true, restore: false)))
    }
    
    func setDirection(for state: GameConstants.PlayerState) {
        if state == .RunRight || state == .JumpRight || state == .IdleRight {
            playerDirection = .right
        } else if state == .RunLeft || state == .JumpLeft || state == .IdleLeft {
            playerDirection = .left
        }
    }
    
    func setAnimation(_ value: Bool) {
        isAnimating = value
    }
    
    func animateOnce(for state: GameConstants.PlayerState, with duration: Double) {
        var flagDirectionChange = false
        if (playerDirection == .left && state == .RunRight) || (playerDirection == .right && state == .RunLeft) {
            flagDirectionChange = true
        }
        if !flagDirectionChange {
            if isAnimating && state.rawValue < 6 { // Only applies for non-death animations
                return
            }
        }
        removeAllActions()
        setDirection(for: state)
        let frames = AnimationHelper.loadTextures(from: animFrameMap[state]!, withName: "\(state)")
        self.setAnimation(true)
        let animation = SKAction.animate(with: frames, timePerFrame: duration, resize: true, restore: false)
        var postAnimationAction = SKAction()
        if state.rawValue > 5 { // only applies for death animations
            postAnimationAction = SKAction.run {
                self.setAnimation(false)
                if let gameScene = self.parent?.scene as? GameScene {
                    gameScene.playerPostAnimation()
                }
                if let gameOverScene = self.parent?.scene as? GameOverScene {
                    gameOverScene.displayOptions()
                }
            }
        } else {
            postAnimationAction = SKAction.run {
                self.setAnimation(false)
            }
        }
        self.run(SKAction.sequence([animation, postAnimationAction]))
    }
}
