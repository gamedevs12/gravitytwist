//
//  WorldSceneSub.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 3/24/19.
//  Copyright © 2019 Narv. All rights reserved.
//

import SpriteKit

class WorldSceneSub: SKSpriteNode {
    
    var sceneManagerDelegate: SceneManagerDelegate?
    var index: Int
    var worldButtons: [SpriteKitButton]
    var worldTexts: [SKLabelNode]
    
    init(size: CGSize, delegate: SceneManagerDelegate, index: Int) {
        self.sceneManagerDelegate = delegate
        self.index = index
        self.worldButtons = [SpriteKitButton]()
        self.worldTexts = [SKLabelNode]()
        super.init(texture: nil, color: UIColor.clear, size: size)
    }
    
    func loadLayout(buttonAction: @escaping (_ idx: Int) -> ()) {
        
        var startIndex: Int
        var endIndex: Int
        if index == 0 {
            startIndex = 1
            endIndex = 3
        } else {
            startIndex = index*3 + 1
            if GameConstants.WorldConstants.totalWorlds < startIndex + 2 {
                endIndex = GameConstants.WorldConstants.totalWorlds
            } else {
                endIndex = startIndex + 2
            }
        }
        
        var xPositionMultiplier: CGFloat
        for i in startIndex...endIndex {
            let world = SpriteKitButton(defaultImageName: "World\(i).png", action: buttonAction, index: i-1)
            switch(i%3) {
            case 1:
                xPositionMultiplier = 0.5
            case 2:
                xPositionMultiplier = 1
            case 0:
                xPositionMultiplier = 1.5
            default:
                debugPrint("Will ever reach this case")
                xPositionMultiplier = 0
            }
            world.position = CGPoint(x: frame.midX*xPositionMultiplier, y: frame.midY)
            world.scale(to: frame.size, width: true, multiplier: 0.2)
            world.zPosition = GameConstants.ZPositions.hudBack
            worldButtons.append(world)
            if GameConstants.enableSave {
                if let worldAccessible = UserDefaults.standard.dictionary(forKey: GameConstants.StringConstants.saveDataName)?["world"] as? Int {
                    if i > worldAccessible {
                        world.alpha = 0.25
                        world.enabled(value: false)
                    }
                }
            }
            let label = SKLabelNode(fontNamed: GameConstants.StringConstants.fontName)
            label.position = CGPoint(x: frame.midX*xPositionMultiplier, y: frame.midY*0.5)
            label.text = GameConstants.WorldConstants.worldIndexToName[i]
            label.fontSize = GameConstants.FloatConstants.worldTextSize
            label.zPosition = GameConstants.ZPositions.hudBack
            worldTexts.append(label)
            addChild(label)
            addChild(world)
        }
        // ( 1/2, 2/2, 3/2)
        // 1/2 => 1,4,7,10,13..
        // 1   => 2,5,8,11,14..
        // 3/4 => 3,6,9,12,15..
    }
    
    func clearLayout() {
        for i in worldButtons {
            i.removeFromParent()
        }
        worldButtons.removeAll()
        for i in worldTexts {
            i.removeFromParent()
        }
        worldTexts.removeAll()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
