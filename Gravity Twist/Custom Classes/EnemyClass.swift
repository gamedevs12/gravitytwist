//
//  EnemyClass.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 11/15/18.
//  Copyright © 2018 Narv. All rights reserved.
//

import SpriteKit

class Enemy: SKSpriteNode {
    
    var idleFrames = [SKTexture]()
    
    func loadTextures(for atlas: String, with prefix: String) {
        idleFrames = AnimationHelper.loadTextures(from: SKTextureAtlas(named: atlas), withName: prefix)
        animate()
    }
    
    func animate() {
        removeAllActions()
        self.run(SKAction.repeatForever(SKAction.animate(with: idleFrames, timePerFrame: 0.05, resize: true, restore: true)))
    }
}
