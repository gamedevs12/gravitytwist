//
//  DialogueClass.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 5/27/21.
//  Copyright © 2021 Narv. All rights reserved.
//

import SpriteKit

class DialogueClass {
    
    // immutable variables
    var dialogueSprite: SKSpriteNode
    var dialogueBackground: SKTexture
    var world: Int
    var level: Int
    
    // mutable variables
    var characterPortrait: [[SKTexture]]
    var dialogueText: [String]
    var currentDialogue = 0
    var isVisible = false
    
    init(world: Int, level: Int) {
        self.world = world
        self.level = level
        self.dialogueSprite = SKSpriteNode()
        self.dialogueBackground = SKTexture(imageNamed: GameConstants.DialogueSequences.dialogueTextBackgroundName)
        self.characterPortrait = AnimationHelper.extractCharInitTextures(world: world, level: level)
        self.dialogueText = ObjectHelper.extractLevelDialogues(world: world, level: level)
    }
    
    func setup() {
        if self.characterPortrait[0].isEmpty || self.dialogueText[0].isEmpty {
            return
        }
        addBackground()
        addPortrait()
        addDialogueText()
        isVisible = true
    }
    
    func addBackground() {
        let sprite = SKSpriteNode(texture: dialogueBackground, color: UIColor.clear, size: dialogueBackground.size())
        sprite.position = CGPoint(x: 0, y: -125)
        sprite.zPosition = GameConstants.ZPositions.player
        dialogueSprite.addChild(sprite)
    }
    
    func addPortrait() {
        let sprite = SKSpriteNode(texture: characterPortrait[0][0], color: UIColor.clear, size: characterPortrait[0][0].size())
        sprite.position = CGPoint(x: -300, y: -97)
        sprite.zPosition = GameConstants.ZPositions.hudBack
        sprite.run(SKAction.repeatForever(SKAction.animate(with: characterPortrait[currentDialogue], timePerFrame: 0.1, resize: false, restore: false)))
        dialogueSprite.addChild(sprite)
    }
    
    func addDialogueText() {
        let dialogue = SKLabelNode(fontNamed: GameConstants.StringConstants.fontName)
        dialogue.text = dialogueText[currentDialogue]
        let diagCount = dialogue.text!.count
        if diagCount < GameConstants.DialogueSequences.charsPerLine {
            dialogue.position = CGPoint(x: 10, y: -100)
        } else if(diagCount > GameConstants.DialogueSequences.charsPerLine && diagCount < GameConstants.DialogueSequences.charsPerLine*2) {
            dialogue.position = CGPoint(x: 10, y: -120)
        } else if(diagCount > GameConstants.DialogueSequences.charsPerLine*2 && diagCount < GameConstants.DialogueSequences.charsPerLine*3) {
            dialogue.position = CGPoint(x: 10, y: -140)
        } else {
            dialogue.position = CGPoint(x: 10, y: -160)
        }
        dialogue.fontColor = UIColor.brown
        dialogue.fontSize = 20
        dialogue.preferredMaxLayoutWidth = 500
        dialogue.lineBreakMode = .byClipping
        dialogue.numberOfLines = 4
        dialogue.zPosition = GameConstants.ZPositions.hudFront
        dialogueSprite.addChild(dialogue)
    }
    
    func nextDialogue(isItemDialogue: Bool) {
        currentDialogue += 1
        removeDialogues()
        if currentDialogue <= dialogueText.count-1 {
            if isItemDialogue {
                characterPortrait = AnimationHelper.extractCharItemDialogueTextures(world: world, level: level)
                dialogueText = ObjectHelper.extractLevelItemDialogues(world: world, level: level)
            }
            setup()
        }
    }
    
    func removeDialogues() {
        for i in self.dialogueSprite.children {
            i.removeFromParent()
        }
        isVisible = false
    }
}
