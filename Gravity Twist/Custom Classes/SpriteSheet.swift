//
//  SpriteSheet.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 2/25/21.
//  Copyright © 2021 Narv. All rights reserved.
//

import SpriteKit

struct SpriteSheet {
    var sheetName: String
    var level: Int
    var world: Int
    var coinAtlas: SKTextureAtlas
    var specialItemLevelMap: [Int:[SKTexture]]
    var staticObstacleMap: [GameConstants.ObjectTypes: [SKTexture]]
    var gravityFields: [Int: SKTextureAtlas]
    var gravityFieldTexures : [Int: [SKTexture]]
    var animatedObjects: [GameConstants.ObjectTypes: [SKTexture]]
    
    init(world: Int, level: Int) {
        self.world = world
        self.level = level
        if let name = GameConstants.TextureConstants.spritesheetWorldMap[world] {
            self.sheetName = name
        } else {
            self.sheetName = String()
        }
        self.coinAtlas = SKTextureAtlas()
        self.specialItemLevelMap = [Int:[SKTexture]]()
        self.staticObstacleMap = [GameConstants.ObjectTypes: [SKTexture]]()
        self.gravityFields = [Int: SKTextureAtlas]()
        self.gravityFieldTexures = [Int : [SKTexture]]()
        self.animatedObjects = [GameConstants.ObjectTypes: [SKTexture]]()
    }
    
    mutating func splitSprites() {
        let cgImg = UIImage(named: sheetName)?.cgImage
        let tileSize = GameConstants.TextureConstants.tileSize
        // Special item map
        for (index, list) in GameConstants.TextureConstants.specialItemLevelMap {
            if index.x == world && index.y == level {
                for item in list {
                    let rect = CGRect(x: item.x*tileSize.x, y: item.y*tileSize.y, width: tileSize.x, height: tileSize.y)
                    let cgImgTemp = cgImg!.cropping(to: rect)
                    let tex = SKTexture(cgImage: cgImgTemp!)
                    if let _ = self.specialItemLevelMap[index.y] {
                        self.specialItemLevelMap[index.y]?.append(tex)
                    } else {
                        self.specialItemLevelMap[index.y] = [SKTexture]()
                        self.specialItemLevelMap[index.y]?.append(tex)
                    }
                }
            }
        }
        
        // coin Atlas
        var coinTex: (start: CGPoint, end: CGPoint)
        if let map = GameConstants.TextureConstants.coinWorldMap[world] {
            coinTex = map
        } else {
            // if no coins texture defined for world use world 0 texture
            coinTex = GameConstants.TextureConstants.coinWorldMap[0]!
        }
        let coinStart = coinTex.start
        let coinEnd = coinTex.end
        let totalFrames = Int(coinEnd.x - coinStart.x)
        var frames = [String:UIImage]()
        for i in 0...totalFrames {
            let xOffset = CGFloat(i)*tileSize.x
            let rect = CGRect(x: coinStart.x*tileSize.x + xOffset, y: coinStart.y*tileSize.y, width: tileSize.x, height: tileSize.y)
            let cgImgTemp = cgImg!.cropping(to: rect)
            frames["coin\(i)"] = UIImage(cgImage: cgImgTemp!)
        }
        coinAtlas = SKTextureAtlas(dictionary: frames)
        
        // gravity field
        let fieldLength = GameConstants.TextureConstants.gravityFieldLength
        var texs : [SKTexture] = [SKTexture]()
        
        for type in 0..<GameConstants.Direction.allCases.count {
            var frames = [String:UIImage]()
            let startPoint = GameConstants.TextureConstants.gravityFieldsMap[GameConstants.Direction(rawValue: type)!]!
            for i in 0..<fieldLength {
                let xOffset = CGFloat(i)*tileSize.x
                let rect = CGRect(x: startPoint.x*tileSize.x + xOffset, y: startPoint.y*tileSize.y, width: tileSize.x, height: tileSize.y)
                let cgImgTemp = cgImg!.cropping(to: rect)
                frames["field\(i)"] = UIImage(cgImage: cgImgTemp!)
                let tex = SKTexture(cgImage: cgImgTemp!)
                texs.append(tex)
            }
            self.gravityFieldTexures[type] = texs
            gravityFields[type] = SKTextureAtlas(dictionary: frames)
        }
        
        // Obstacles
        for (i, location) in GameConstants.TextureConstants.staticObstacleMap.enumerated() {
            let xOffset = CGFloat(i)*tileSize.x
            let rect = CGRect(x: location.value.x*tileSize.x + xOffset, y: location.value.y*tileSize.y, width: tileSize.x, height: tileSize.y)
            let cgImgTemp = cgImg!.cropping(to: rect)
            let tex = SKTexture(cgImage: cgImgTemp!)
            staticObstacleMap[GameConstants.ObjectTypes.spikes] = [tex]
        }
        texs.removeAll()
        for (i, location) in GameConstants.TextureConstants.animatedObjectsMap.enumerated() {
            let start = location.value[0]
            let end = location.value[1]
            let length = Int(end.x - start.x)
            for j in 0...length {
                let xOffset = CGFloat(j)*tileSize.x
                let rect = CGRect(x: start.x*tileSize.x + xOffset, y: start.y*tileSize.y, width: tileSize.x, height: tileSize.y)
                let cgImgTemp = cgImg!.cropping(to: rect)
                frames["\(location.key)\(i)"] = UIImage(cgImage: cgImgTemp!)
                let tex = SKTexture(cgImage: cgImgTemp!)
                texs.append(tex)
            }
            self.animatedObjects[location.key] = texs
            texs.removeAll()
        }
        
    }
    
}
