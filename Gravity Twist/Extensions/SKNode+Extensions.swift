//
//  SKNode+Extensions.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 2/1/18.
//  Copyright © 2018 Narv. All rights reserved.
//

import SpriteKit

extension SKNode {
    
    func uniformScale(to size: CGSize, multiplier: CGFloat) {
        let xScale = (size.width * multiplier)/(self.frame.size.width)
        let yScale = (size.height * multiplier)/(self.frame.size.height)
        self.xScale = xScale
        self.yScale = yScale
    }
    
    func scale(to size: CGSize, width: Bool,multiplier: CGFloat) {
        let scale = width ? (size.width * multiplier)/(self.frame.size.width) : (size.height * multiplier)/(self.frame.size.height)
        self.setScale(scale)
    }
    
    func turnGravityOn(value: Bool) {
        self.physicsBody?.affectedByGravity = value
    }
}
