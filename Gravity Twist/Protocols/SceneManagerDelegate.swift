//
//  File.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 11/11/18.
//  Copyright © 2018 Narv. All rights reserved.
//

import Foundation

protocol SceneManagerDelegate {
    
    func presentIntroScene()
    func presentMenuScene()
    func presentWorldScene()
    func presentLevelScene(for level: Int, in world: Int)
    func presentGameScene(for level: Int, in world: Int)
    func presentGameOverScene(for level: Int, in world: Int, with state: GameConstants.PlayerState)
    func presentAchievementScene()
    func presentCreditsScene()
    func presentTransitionScene(nextLevel: Int, nextWorld: Int)
}
