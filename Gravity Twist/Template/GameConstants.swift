//
//  GameConstants.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 1/30/18.
//  Copyright © 2018 Narv. All rights reserved.
//

import SpriteKit

struct GameConstants{
    
    struct StringConstants {
        static let titleName: String = "Gravity Phroid"
        static let startButtonImage: String = "StartButton.png"
        static let startButtonPlainImage: String = "StartButton-Plain.png"
        static let mainMenuBg: String = "MainMenuBg.png"
        
        static let bwTilesName: String = "BW Tiles"
        static let transitionTilesName: String = "TransitionTiles"
        static let playerName: String = "Player"
        static let enemyName: String = "Enemy"
        static let trapName: String = "Trap"
        static let startPoint: String = "Start"
        static let gravityField: String = "GravityField"
        static let bossName: String = "Boss"
        static let spikesName: String = "Spikes"
        static let spikesGroupName: String = "SpikesGroup"
        static let nonTilePlatformName: String = "Platform"
        static let leverName: String = "Lever"
        static let rockName: String = "Rock"
        static let rockSpawnName: String = "RockSpawn"
        static let rockGeneratorName: String = "RockGenerator"
        static let rockGroupName: String = "RockGroup"
        static let colliderName: String = "Collider"
        static let movingPlatformName: String = "MovingPlatform"
        static let bossRageName: String = "BossRage"
        static let bossStartName: String = "BossStart"
        static let bossEndName: String = "BossEnd"
        static let bossRangeName: String = "BossRange"
        static let warningName: String = "Warning"
        static let optionButtonName: String = "OptionMenu_Button"
        static let pauseButtonName: String = "Pause-Button"
        // PLAYER ATLAS
        static let playerIdleAtlas = "Player Idle Atlas"
        static let playerRunAtlas = "Player Run Atlas"
        static let playerJumpAtlas = "Player Jump Atlas"
        static let playerDieAtlas = "Player Die Atlas"
        static let idlePrefixKey = "Idle_"
        static let runPrefixKey = "Run_"
        static let jumpPrefixKey = "Jump_"
        static let diePrefixKey = "Die_"
        static let evelynSpriteSheetName = "Evelyn-Chibi"
        // ENEMY ATLAS
        static let BeeIdleAtlas = "Bee"
        static let BeeidlePrefixKey = "Bee_"
        static let BatIdleAtlas = "Bat"
        static let BatidlePrefixKey = "bat_"
        static let BladeIdleAtlas = "Blade"
        static let BladeidlePrefixKey = "blade_"
        static let FlameIdleAtlas = "Flame"
        static let FlameidlePrefixKey = "flame_"
        static let MadflyIdleAtlas = "Madfly"
        static let MadflyidlePrefixKey = "madfly_"
        
        static let coinName: String = "coin"
        static let doorName: String = "doorway"
        
        static let fontName: String = "LittleMissLoudon-Bold"
        
        static let saveDataName: String = "GTSavedData"
        
        static let rateLink: String = "https://apps.apple.com/us/app/gravity-phroid/id1575237652"
        static let siteLink: String = "https://vectorhashmap.wixsite.com/home"
        static let bugReportLink: String = "https://docs.google.com/forms/d/e/1FAIpQLSfMxrTpz19jxGKAFzc3EfX_V51UH5WrCzgpT4_ou5yIa5RJWg/viewform?usp=pp_url"
        
        static let gameplayMusicName: String = "GameplayMusic"
        static let menuMusicName: String = "MenuMusic"
        static let bossMusicName: String = "BossMusic"
        static let victoryMusicName: String = "VictoryMusic"
        
        static let introHeading = "Gravity Twist"
        static let introText = ["In the world of humans and phroids, conflicts arise often due to their differences.", "After an armistice was signed, both races were peace until this day.", "A phroid mastermind, TwoBit, has captured all humans. And wishes to dominate the world.", "Meanwhile, a phroid and a human were peacefully sleeping in bed. The unique couple who spread love between the species.", "You play the phroid named Evelyn who wakes up in the middle of the night."]
        static let tapText = "Tap to Continue"
        
       static let credits = ["Created, Directed, Programmed and Art by: \n\t\t\t\t\tAbhinav Rathod"
       ,"This would not have been possible without the constant support from my wife Nikitha"
       ,"Thanks to these guys, the best game dev team i know, inspired me:"
       ,"Mike Zyda, Joel Clark, Michael Longley and Rayvionne French"
       ,"Finally, I would like to thank my friends/faculty from USC who helped me make a simple version of this game in 2013:"
       ,"Arpit Bansal, Daniel Kessler, Pramodh Aravindan, Scott Easley & Jerry Lin"
       ,"Hope you enjoyed playing this game. There are a total of 18 worlds. New worlds coming soon. Stay Tuned!"]
    }
    
    struct Sound {
        static let bossEnd = "bossEnd.mp3"
        static let gameOver = "GameOver.mp3"
        static let coin = "coinPickup.mp3"
        static let lever = "MetalLever.mp3"
        static let door = "DoorOpenClose.mp3"
    }
    
    struct PhysicsCategories {
        static let noCategory: UInt32 = 0
        static let allCategory: UInt32 = UInt32.max
        static let playerCategory: UInt32 = 0x1
        static let groundCategory: UInt32 = 0x1 << 1
        static let enemyCategory: UInt32 = 0x1 << 2
        static let trapCategory: UInt32 = 0x1 << 3
        static let collectibleCategory: UInt32 = 0x1 << 4
        static let frameCategory: UInt32 = 0x1 << 5
        static let finishCategory: UInt32 = 0x1 << 6
        static let gravityCategory: UInt32 = 0x1 << 7
        static let obstacleCategory: UInt32 = 0x1 << 8
        static let interactCategory: UInt32 = 0x1 << 9
        static let miscCategory: UInt32 = 0x1 << 10
    }
    
    enum TileType {
        case horizontal, vertical, SlopeL, SlopeR, SlopeLH, SlopeLL, SlopeRH, SlopeRL, SlopeLB, SlopeRB, SlopeLBH, SlopeLBL, SlopeRBH, SlopeRBL
    }
    
    struct ZPositions {
        static let background: CGFloat = 0
        static let platform: CGFloat = 1
        static let world: CGFloat = 2
        static let object: CGFloat = 3
        static let player: CGFloat = 4
        static let abovePlayer: CGFloat = 5
        static let hudBack: CGFloat = 6
        static let hudFront: CGFloat = 7
    }
    
    struct WorldConstants {
        static let totalWorlds: Int = 5
        static let totalWorldSubs: Int = totalWorlds/3 + (totalWorlds%3 == 0 ? 0 : 1)
        static let totalSupportedWorlds: Int = 0 // 0 indexed
        static let worldIndexToName = [1:"Home", 2:"Park - Coming Soon!", 3:"Forest - Coming Soon!", 4:"Village - Coming Soon!", 5:"Docks - Coming Soon!"]//, 6:"Coming Soon!", 7:"Coming Soon!", 8:"Coming Soon!", 9:"Coming Soon!", 10:"Coming Soon!", 11:"Coming Soon!", 12:"Coming Soon!", 13:"Coming Soon!", 14:"Coming Soon!", 15:"Coming Soon!", 16:"Coming Soon!", 17:"Coming Soon!", 18:"Coming Soon!", 19:"The Chase", 20:"Highway", 21:"Train", 22:"Bridge"]
        //static let worldIndexToName = [1:"Home", 2:"Park", 3:"Forest", 4:"Village", 5:"Docks", 6:"Ship", 7:"Island", 8:"Cliffs", 9:"Caves", 10:"Undersea Caverns", 11:"City Of Ruin", 12:"The Stairway", 13:"The Trap", 14:"Prison Camp", 15:"Security Checkpoint", 16:"Court House", 17:"Escape", 18:"Factory", 19:"The Chase", 20:"Highway", 21:"Train", 22:"Bridge"]
    }
    
    struct FloatConstants {
        static let fontSize: CGFloat = 150.0
        static let worldTextSize: CGFloat = 20.0
    }
    
    enum enemyMoveType {
        case UpDown, Curve, Diagonal, FlyLeft, FlyRight
    }
    
    enum flameMoveType: Int {
        case UpDown = 0, DownUp = 1, LeftRight = 2, RightLeft = 3, curveCircle = 4, curveBotLeft = 5, curveBotRight = 6, curveTopLeft = 7, curveTopRight = 8, curveLeftUp = 9, curveLeftDown = 10, curveRightUp = 11, curveRightDown = 12
    }
    
    struct WorldLevelMapping {
        static let map = [0:53, 1:21, 2:0, 3:0, 4:0]
    }
    
    struct LevelConstants {
        static let levelSubcount = 15
        static let levelShiftMap =
            [Point(x: 0, y: 31): [0:[CGPoint(x: 0, y: 150)]],
             Point(x: 0, y: 32): [0:[CGPoint(x: 0, y: 100), CGPoint(x: 0, y: -100)]],
             Point(x: 0, y: 33): [0:[CGPoint(x: -100, y: 100)]],
             Point(x: 0, y: 34): [0:[CGPoint(x: 100, y: 0)]],
             Point(x: 0, y: 36): [0:[CGPoint(x: -100, y: 0)]],
             Point(x: 0, y: 37): [0:[CGPoint(x: -100, y: 0)], 1:[CGPoint(x: 0, y: -100)]
                                  ,2:[CGPoint(x: 200, y: 0)], 3:[CGPoint(x: 0, y: 200)]
                                  ,4:[CGPoint(x: -200, y: 0)], 5:[CGPoint(x: 100, y: -100)]]]
        static let leverActionMap =
            [Point(x: 0, y: 37): true]
        static let defaultActionDuration = 3.0
        static let levelShiftDurationMap =
            [Point(x: 0, y: 31): 2.5,
             Point(x: 0, y: 33): 5.0,
             Point(x: 0, y: 34): 4.0]
        static let levelShiftInitMap = [Point(x: 0, y: 34)]
        static let levelShiftOnceMap = [Point(x: 0, y: 37)]
    
        static let transitionSks = "Transition"
    }
    
    struct Point : Hashable {
        let x: Int
        let y: Int
    }
    
    enum Direction: Int, CaseIterable {
        case up = 0, down = 1, right = 2, left = 3
    }
    
    enum ObjectTypes: Int, CaseIterable {
        case spikes = 0, leveron, leveroff, note
    }
    
    enum PlayerState: Int, CaseIterable {
        case IdleRight = 0, IdleLeft = 1, RunRight = 2, RunLeft = 3, JumpRight = 4, JumpLeft = 5, DeathFrame = 6, DeathSpike = 7, DeathRock = 8, DeathRobot = 9, DeathSuction = 10
    }
    
    struct TextureConstants {
        static let spritesheetWorldMap = [0:"Items",1:"Items"]
        static let tileSize = CGPoint(x: 32*5, y: 32*5) // 5 = pixel scale on export
        static let specialItemLevelMap =
            [Point(x: 0, y: 1):[CGPoint(x: 0, y: 0)],
             Point(x: 0, y: 2):[CGPoint(x: 1, y: 0)],
             Point(x: 0, y: 3):[CGPoint(x: 2, y: 0), CGPoint(x: 3, y: 1)],
             Point(x: 0, y: 4):[CGPoint(x: 5, y: 0), CGPoint(x: 5, y: 1)],
             Point(x: 0, y: 5):[CGPoint(x: 6, y: 0)]] as [Point : [CGPoint]]
        static let staticObstacleMap = [ObjectTypes.spikes: CGPoint(x: 0, y: 1)]
        static let animatedObjectsMap = [
            ObjectTypes.leveron: [CGPoint(x: 0, y: 6), CGPoint(x: 6, y: 6)],
            ObjectTypes.leveroff: [CGPoint(x: 0, y: 7), CGPoint(x: 6, y: 7)],
            ObjectTypes.note: [CGPoint(x: 8, y: 1), CGPoint(x: 15, y: 1)]]
        static let levelItemRandomize = [Point(x: 0, y: 4)]
        static let coinWorldMap = [0:(start: CGPoint(x: 8, y: 0), end:CGPoint(x: 15, y: 0))]
        static let gravityFieldsMap = [Direction.up:CGPoint(x: 0, y: 2),
                                       Direction.down:CGPoint(x: 0, y: 3),
                                       Direction.right:CGPoint(x: 0, y: 4),
                                       Direction.left:CGPoint(x: 0, y: 5)]
        static let gravityFieldLength = 10
        // order: 0:UP, 1:DOWN, 2:RIGHT, 3:LEFT ;   4: up, right : 1, left: 2, down: 3
        static let fieldTypeGestureMap : [UInt:UInt] = [0:4, 1:8, 2:1, 3:2]
        static let defaultAnimationSpeed = 0.1
        
        //Player Sprites
        static let playerTileSize = CGPoint(x: 64, y: 64)
        static let playerAnimMap = [
            PlayerState.IdleRight: [CGPoint(x: 0, y: 0), CGPoint(x: 7, y: 0)],
            PlayerState.IdleLeft: [CGPoint(x: 0, y: 8), CGPoint(x: 7, y: 8)],
            PlayerState.RunRight: [CGPoint(x: 0, y: 1), CGPoint(x: 7, y: 1)],
            PlayerState.RunLeft: [CGPoint(x: 0, y: 9), CGPoint(x: 7, y: 9)],
            PlayerState.JumpRight: [CGPoint(x: 0, y: 2), CGPoint(x: 0, y: 2)],
            PlayerState.JumpLeft: [CGPoint(x: 0, y: 10), CGPoint(x: 0, y: 10)],
            PlayerState.DeathFrame: [CGPoint(x: 0, y: 3), CGPoint(x: 9, y: 3)],
            PlayerState.DeathSpike: [CGPoint(x: 0, y: 4), CGPoint(x: 9, y: 4)],
            PlayerState.DeathRock: [CGPoint(x: 0, y: 5), CGPoint(x: 9, y: 5)],
            PlayerState.DeathRobot: [CGPoint(x: 0, y: 6), CGPoint(x: 9, y: 6)],
            PlayerState.DeathSuction: [CGPoint(x: 0, y: 7), CGPoint(x: 14, y: 7)]
            ]
        
        static let doorTextureName = "door-color"
        static let doorAnimationSheetName = "DoorAnimationx3"
        static let doorAnimFrameSize = CGPoint(x: 128*3, y: 72*3)
        static let doorAnimFrameCount = 8
    }
    
    enum BossTypes: Int {
        case RobotArm = 0, Suction = 1
    }
    
    struct BossConstants {
        static let worldBossMap : [Int: [Int: BossTypes]] =
            [0: [28:BossTypes.RobotArm, 75:BossTypes.Suction]]
        static let tileSize = CGPoint(x: 64*3, y: 64*3)
        static let robotArmFrames =
            [BossState.moveleft: [CGPoint(x: 0, y: 0), CGPoint(x: 7, y: 0)]
            ,BossState.moveright: [CGPoint(x: 0, y: 1), CGPoint(x: 7, y: 1)]
            ,BossState.rageleft: [CGPoint(x: 0, y: 2), CGPoint(x: 3, y: 2)]
            ,BossState.rageright: [CGPoint(x: 0, y: 3), CGPoint(x: 3, y: 3)]
            ,BossState.burstleft: [CGPoint(x: 0, y: 4), CGPoint(x: 7, y: 4)]
            ,BossState.burstright: [CGPoint(x: 0, y: 5), CGPoint(x: 7, y: 5)]]
        static let suctionFrames =
            [BossState.idle: [CGPoint(x: 0, y: 0), CGPoint(x: 3, y: 0)]
            ,BossState.idlerage: [CGPoint(x: 4, y: 2), CGPoint(x: 7, y: 2)]
             ,BossState.appear: [CGPoint(x: 0, y: 1), CGPoint(x: 3, y: 1)]
             ,BossState.appearrage: [CGPoint(x: 0, y: 3), CGPoint(x: 3, y: 3)]
             ,BossState.disappear: [CGPoint(x: 0, y: 4), CGPoint(x: 7, y: 4)]
             ,BossState.disappearrage: [CGPoint(x: 0, y: 5), CGPoint(x: 7, y: 5)]
             ,BossState.extra1: [CGPoint(x: 5, y: 1), CGPoint(x: 5, y: 1)]
             ,BossState.extra2: [CGPoint(x: 4, y: 1), CGPoint(x: 4, y: 1)]]
    }
    
    struct SaveData: Codable {
        var world: Int
        var level: Int
    }
    
    struct SavedData {
        static let worldLevelMap = ["world": 1, "level": 1] // default
    }
    
    static let enableSave = false
    
    enum CharacterMood: Int, CaseIterable {
        case neutral = 0, angry, sad, shock, happy, unknown
    }
    
    struct DialogueSequences {
        static let animationSheetName = "DialoguePortraits"
        static let dialogueTextBackgroundName = "DialogueTextFrame"
        static let levelItemDialogueMap = [
            Point(x: 0, y: 1): ["Ahh that feels better"],
            Point(x: 0, y: 2): ["Tom? Where did you go off to?"],
            Point(x: 0, y: 3): ["A Shoe? What is it doing here?"],
            Point(x: 0, y: 4): ["My precious plates! Tom, how dare you break my plates!"],
            Point(x: 0, y: 5): ["A note?", "Evelyn, We have your Tom and the entire human race captured. You are one of us, join us and lets start a brand new world! We syphers have our own identity and will not be undermined by mere humans. -TwoBit", "The war ended between our races ended long ago and now TwoBit wants to start it again? I won’t let this happen! Need to rescue my love and destroy this TwoBit."]
        ]
        static let levelItemMoodMap = [
            Point(x: 0, y: 1): [CharacterMood.happy],
            Point(x: 0, y: 2): [.neutral],
            Point(x: 0, y: 3): [.neutral],
            Point(x: 0, y: 4): [.angry],
            Point(x: 0, y: 5): [.neutral, .unknown, .angry]
        ]
        static let levelDiagMap = [
            Point(x: 0, y: 1): ["Sooo Thirstyyyy….!"],
            Point(x: 0, y: 2): ["Time to go back to sleep"],
            Point(x: 0, y: 4): ["Toommm..! Where are you?"],
            Point(x: 0, y: 7): ["Oh the arrows show where i can shift gravity, sweet!"],
            Point(x: 0, y: 8): ["Ah, the shifting only works only at these arrow things."],
            Point(x: 0, y: 28): ["HUH?! What is that THING doing in MY HOME?", "Someones gonna pay for the damages!"],
            Point(x: 0, y: 33): ["OOO, I see a fancy Lever! Lets see what it does."],
            Point(x: 0, y: 40): ["They installed rock generators? Dammit TwoBit!"],
            Point(x: 0, y: 53): ["Final Level, huh? Seems easy..","Uh-oh I see someting overhead!"]
        ]
        static let levelCharMoods = [
            Point(x: 0, y: 1): [CharacterMood.sad],
            Point(x: 0, y: 2): [.happy],
            Point(x: 0, y: 4): [.neutral],
            Point(x: 0, y: 7): [.happy],
            Point(x: 0, y: 8): [.angry],
            Point(x: 0, y: 28): [.shock, .angry],
            Point(x: 0, y: 33): [.happy],
            Point(x: 0, y: 40): [.angry],
            Point(x: 0, y: 53): [.happy, .shock],
        ]
        static let frameSize = CGPoint(x: 32*2, y: 32*2)
        static let charAnimMoodMap = [
            CharacterMood.neutral: [CGPoint(x: 0, y: 0), CGPoint(x: 3, y: 0)],
            .angry: [CGPoint(x: 0, y: 1), CGPoint(x: 3, y: 1)],
            .sad: [CGPoint(x: 0, y: 2), CGPoint(x: 3, y: 2)],
            .shock: [CGPoint(x: 0, y: 3), CGPoint(x: 3, y: 3)],
            .unknown: [CGPoint(x: 0, y: 4), CGPoint(x: 3, y: 4)],
            .happy: [CGPoint(x: 0, y: 5), CGPoint(x: 3, y: 5)]
        ]
        static let charsPerLine = 62
    }
    
    struct Tutorial {
        static let moduleName = "Tutorial"
        static let frameName = "TutorialTextFrame"
        static let introSaveKey = "firstTime"
        static let levelStart = [
            Point(x: 0, y: 1)
        ]
        static let longPress = [
            Point(x: 0, y: 2)
        ]
        static let moreThanSecondLevel = Point(x: 0, y: 3)
        static let levelTextMap = [
            Point(x: 0, y: 1): ["Tap, hold and pan to move left/right", "Lift finger to stop moving"],
            Point(x: 0, y: 2): ["Tap pause button at top right corner"],
            Point(x: 0, y: 6): ["Swipe up", "Swipe down"],
            Point(x: 0, y: 7): ["Swipe right", "Swipe up"],
            Point(x: 0, y: 8): ["Swipe down", "Swipe up", "Swipe down"]
        ]
    }
    
    enum MusicType {
        case menu, gameplay, boss, victory
    }
    
    struct Achievement {
        static let congratsArray = ["Good job!", "Awesome job!", "Way to go!", "Fantastic!", "You rock!", "Nice going!", "Well done!", "Yay!", "Keep going!", "Thats the way to do it", "Onto the next!", "Sweet!",
            "Not bad!", "Someones got game!", "That was easy!", "Easy as pie", "Woot!"]
        static let higherCongratsArray = ["You did it!", "Nothing can stop you now!", "You made it look too easy!", "Bravo!", "An Excellent job!", "Took the Dev forever to beat this one!", "WOW! Thats a great win!", "You've made history!", "Hats off!", "Perfection!", "SUPERB!", "Sensational!", "Impressive!", "Brilliant!"]
    }
    
    static var enableButton = true
    
    struct Credits {
        /*
         * Created, Directed, Programmed and Art by: Abhinav Rathod
         * This would not have been possible without the constant support by my wife Nikitha
         * I would like to thank my friends/faculty from USC who helped me
         * make a simple version of this game in 2013:
         * Arpit Bansal
         * Daniel Kessler
         * Pramodh Aravindan
         * Scott Easley
         * Jerry Lin
         
         * I would also like to thank for inspiration to make games that led me to make this one:
         * Mike Zyda
         * Joel
         * Michael
         * Ray
         
         * Hope you enjoyed playing this game. There are 18 worlds in total i plan to make.
         * Will release them one by one soon!
         */
    }
}

/*
 * Notes:
 
 * Levels to remove: 6, 10-13, 15, 17(too hard), 18, 20, 22, 23, 30, 31, 34, 35, 37, 43, 45, 50, 60, 63, 68,
 * Levels Removed! 75 -> 53 , 22 levels removed wow.
 */

extension Dictionary where Value: Equatable {
    func allKeys(forValue val: Value) -> [Key] {
        return self.filter { $1 == val }.map { $0.0 }
    }
}
