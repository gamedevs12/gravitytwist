//
//  GroundHelper.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 4/8/18.
//  Copyright © 2018 Narv. All rights reserved.
//

import SpriteKit

class GroundNode: SKSpriteNode {
    
    // Need to use didSimulatePhysics(in gamescene)
    // to set physicsbody ondemand to save rss
    var isBodyActivated: Bool = false {
        didSet {
            physicsBody = isBodyActivated ? activatedBody: nil
        }
    }
    
    private var activatedBody: SKPhysicsBody?
    
    init(with size: CGSize, type value: GameConstants.TileType) {
        super.init(texture:nil, color: UIColor.clear, size: size)
        
        var tagInfo: String
        var bodyInitialPoint: CGPoint
        var bodyEndPoint: CGPoint
        switch value {
        case GameConstants.TileType.vertical:
            bodyInitialPoint = CGPoint(x: size.width, y: 0.0)
            bodyEndPoint = CGPoint(x: size.width, y: size.height)
            tagInfo = "vertical"
        case GameConstants.TileType.horizontal:
            bodyInitialPoint = CGPoint(x: 0.0, y: size.height)
            bodyEndPoint = CGPoint(x: size.width, y: size.height)
            tagInfo = "horizontal"
        case GameConstants.TileType.SlopeL:
            bodyInitialPoint = CGPoint(x: 0.0, y: 0.0)
            bodyEndPoint = CGPoint(x: size.width, y: size.height)
            tagInfo = "Slope"
        case GameConstants.TileType.SlopeR:
            bodyInitialPoint = CGPoint(x: size.width, y: 0.0)
            bodyEndPoint = CGPoint(x: 0.0, y: size.height)
            tagInfo = "Slope"
        case GameConstants.TileType.SlopeLL:
            bodyInitialPoint = CGPoint(x: 0.0, y: 0.0)
            bodyEndPoint = CGPoint(x: size.width, y: size.height/2)
            tagInfo = "Slope"
        case GameConstants.TileType.SlopeLH:
            bodyInitialPoint = CGPoint(x: 0.0, y: size.height/2)
            bodyEndPoint = CGPoint(x: size.width, y: size.height)
            tagInfo = "Slope"
        case GameConstants.TileType.SlopeRL:
            bodyInitialPoint = CGPoint(x: size.width, y: 0)
            bodyEndPoint = CGPoint(x: 0.0, y: size.height/2)
            tagInfo = "Slope"
        case GameConstants.TileType.SlopeRH:
            bodyInitialPoint = CGPoint(x: size.width, y: size.height/2)
            bodyEndPoint = CGPoint(x: 0.0, y: size.height)
            tagInfo = "Slope"
        case GameConstants.TileType.SlopeLB:
            bodyInitialPoint = CGPoint(x: 0.0, y: 0.0)
            bodyEndPoint = CGPoint(x: size.width, y: size.height)
            tagInfo = "Slope"
        case GameConstants.TileType.SlopeRB:
            bodyInitialPoint = CGPoint(x: size.width, y: 0.0)
            bodyEndPoint = CGPoint(x: 0.0, y: size.height)
            tagInfo = "Slope"
        case GameConstants.TileType.SlopeLBH:
            bodyInitialPoint = CGPoint(x: 0.0, y: 0.0)
            bodyEndPoint = CGPoint(x: size.width, y: size.height/2)
            tagInfo = "Slope"
        case GameConstants.TileType.SlopeLBL:
            bodyInitialPoint = CGPoint(x: 0.0, y: size.height/2)
            bodyEndPoint = CGPoint(x: size.width, y: size.height)
            tagInfo = "Slope"
        case GameConstants.TileType.SlopeRBH:
            bodyInitialPoint = CGPoint(x: 0.0, y: size.height/2)
            bodyEndPoint = CGPoint(x: size.width, y: 0)
            tagInfo = "Slope"
        case GameConstants.TileType.SlopeRBL:
            bodyInitialPoint = CGPoint(x: 0.0, y: size.height)
            bodyEndPoint = CGPoint(x: size.width, y: size.height/2)
            tagInfo = "Slope"
        }
        
        activatedBody = SKPhysicsBody(edgeFrom: bodyInitialPoint, to: bodyEndPoint)
        activatedBody!.restitution = 0.0
        activatedBody!.categoryBitMask = GameConstants.PhysicsCategories.groundCategory
        activatedBody!.collisionBitMask = GameConstants.PhysicsCategories.playerCategory
        
        physicsBody = activatedBody
        name = tagInfo
        zPosition = GameConstants.ZPositions.platform
    }
    
    // to help copying GroundNode
    override init(texture: SKTexture?, color c: UIColor, size s: CGSize)
    {
        super.init(texture: texture, color: c, size: s)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
