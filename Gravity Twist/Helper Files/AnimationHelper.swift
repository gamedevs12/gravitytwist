//
//  AnimationHelper.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 10/20/18.
//  Copyright © 2018 Narv. All rights reserved.
//

import SpriteKit

class AnimationHelper {
    
    static func loadTextures(from atlas: SKTextureAtlas, withName name: String) -> [SKTexture] {
        var textures = [SKTexture]()
        
        for i in 0..<atlas.textureNames.count {
            let textureName = name + String(i)
            textures.append(atlas.textureNamed(textureName))
        }
        
        return textures
    }
    
    static func fetchDialogueFrames(mood: [GameConstants.CharacterMood]) -> [[SKTexture]] {
        let cgImg = UIImage(named: GameConstants.DialogueSequences.animationSheetName)?.cgImage
        let tileSize = GameConstants.DialogueSequences.frameSize
        var arrayOfAnims = [[SKTexture]]()
        for i in mood {
            let info = GameConstants.DialogueSequences.charAnimMoodMap[i]!
            let framesCount = Int(info[1].x - info[0].x)
            var frames = [SKTexture]()
            for j in 0...framesCount {
                let rect = CGRect(x: CGFloat(j)*tileSize.x, y: info[0].y*tileSize.y, width: tileSize.x, height: tileSize.y)
                let cgImgTemp = cgImg!.cropping(to: rect)
                frames.append(SKTexture(cgImage: cgImgTemp!))
            }
            arrayOfAnims.append(frames)
        }
        return arrayOfAnims
    }
    
    static func extractCharInitTextures(world: Int, level: Int) -> [[SKTexture]] {
        var frames = [[SKTexture]()]
        if let mood = GameConstants.DialogueSequences.levelCharMoods[GameConstants.Point(x: world, y: level)] {
            frames = fetchDialogueFrames(mood: mood)
        }
        return frames
    }
    
    static func extractCharItemDialogueTextures(world: Int, level: Int) -> [[SKTexture]] {
        var frames = [[SKTexture]()]
        if let mood = GameConstants.DialogueSequences.levelItemMoodMap[GameConstants.Point(x: world, y: level)] {
            frames = fetchDialogueFrames(mood: mood)
        }
        return frames
    }
    
    static func getDoorAnimation() -> [SKTexture] {
        let cgImg = UIImage(named: GameConstants.TextureConstants.doorAnimationSheetName)?.cgImage
        let tileSize = GameConstants.TextureConstants.doorAnimFrameSize
        let frameCount = GameConstants.TextureConstants.doorAnimFrameCount
        var frames = [SKTexture]()
        
        for i in 0..<frameCount {
            let rect = CGRect(x: CGFloat(i)*tileSize.x, y: 0, width: tileSize.x, height: tileSize.y)
            let cgImgTemp = cgImg!.cropping(to: rect)
            frames.append(SKTexture(cgImage: cgImgTemp!))
        }
        
        return frames
    }
}
