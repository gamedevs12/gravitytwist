//
//  PhysicsHelper.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 4/4/18.
//  Copyright © 2018 Narv. All rights reserved.
//

import SpriteKit

class PhysicsHelper {
    
    static func addPhysics(_ sprite: SKSpriteNode)
    {
        switch sprite.name! {
        case GameConstants.StringConstants.playerName:
            sprite.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: sprite.size.width/5, height: sprite.size.height/1.7))
            sprite.physicsBody!.restitution = 0.0
            sprite.physicsBody!.allowsRotation = false
            sprite.physicsBody!.friction = 0.8
            sprite.physicsBody!.categoryBitMask = GameConstants.PhysicsCategories.playerCategory
            sprite.physicsBody!.collisionBitMask = GameConstants.PhysicsCategories.groundCategory | GameConstants.PhysicsCategories.obstacleCategory
            sprite.physicsBody!.contactTestBitMask = GameConstants.PhysicsCategories.groundCategory | GameConstants.PhysicsCategories.finishCategory | GameConstants.PhysicsCategories.frameCategory
            sprite.physicsBody!.isDynamic = true
        case GameConstants.StringConstants.enemyName:
            sprite.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: sprite.size.width, height: sprite.size.height))
            sprite.physicsBody!.affectedByGravity = false
            sprite.physicsBody!.restitution = 0.0
            sprite.physicsBody!.categoryBitMask = GameConstants.PhysicsCategories.enemyCategory
            sprite.physicsBody!.collisionBitMask = GameConstants.PhysicsCategories.groundCategory
        case GameConstants.StringConstants.bossRageName:
            fallthrough
        case GameConstants.StringConstants.bossName:
            addBossPhysics(sprite)
        case GameConstants.StringConstants.coinName, GameConstants.StringConstants.doorName, GameConstants.StringConstants.gravityField:
            if( sprite.name! == GameConstants.StringConstants.coinName ) {
                sprite.physicsBody = SKPhysicsBody(circleOfRadius: sprite.size.width/2)
                sprite.physicsBody!.categoryBitMask = GameConstants.PhysicsCategories.collectibleCategory
            } else {
                if(sprite.alpha < 1.0) {
                    return
                }
                sprite.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: sprite.size.width*0.8, height: sprite.size.height*0.8))
                sprite.physicsBody!.categoryBitMask = GameConstants.PhysicsCategories.finishCategory
            }
            sprite.physicsBody!.isDynamic = false
            sprite.physicsBody!.affectedByGravity = false
            sprite.physicsBody!.allowsRotation = false
            if sprite.name == GameConstants.StringConstants.gravityField {
                sprite.physicsBody!.categoryBitMask = GameConstants.PhysicsCategories.gravityCategory
                sprite.physicsBody!.collisionBitMask = GameConstants.PhysicsCategories.noCategory
            } else {
                sprite.physicsBody!.collisionBitMask = GameConstants.PhysicsCategories.playerCategory
            }
            sprite.physicsBody!.contactTestBitMask = GameConstants.PhysicsCategories.allCategory
            sprite.physicsBody!.restitution = 0.0
        case GameConstants.StringConstants.leverName:
            sprite.physicsBody = SKPhysicsBody(rectangleOf: sprite.size)
            sprite.physicsBody!.affectedByGravity = false
            sprite.physicsBody!.allowsRotation = false
            sprite.physicsBody!.isDynamic = false
            sprite.physicsBody!.restitution = 0.0
            sprite.physicsBody!.categoryBitMask = GameConstants.PhysicsCategories.interactCategory
            sprite.physicsBody!.contactTestBitMask = GameConstants.PhysicsCategories.playerCategory
            sprite.physicsBody!.collisionBitMask = GameConstants.PhysicsCategories.allCategory
        case GameConstants.StringConstants.rockName:
            sprite.physicsBody = SKPhysicsBody(circleOfRadius: sprite.size.width/2)
            sprite.physicsBody!.mass = 0.5
            sprite.physicsBody!.restitution = 0.3
            sprite.physicsBody!.affectedByGravity = true
            sprite.physicsBody!.isDynamic = true
            sprite.physicsBody!.categoryBitMask = GameConstants.PhysicsCategories.obstacleCategory
            sprite.physicsBody!.collisionBitMask = GameConstants.PhysicsCategories.groundCategory | GameConstants.PhysicsCategories.playerCategory
            sprite.physicsBody!.contactTestBitMask = GameConstants.PhysicsCategories.miscCategory | GameConstants.PhysicsCategories.playerCategory
        case GameConstants.StringConstants.rockSpawnName:
            sprite.physicsBody = SKPhysicsBody(rectangleOf: sprite.size)
            sprite.physicsBody!.affectedByGravity = false
            sprite.physicsBody!.allowsRotation = false
            sprite.physicsBody!.isDynamic = false
            sprite.physicsBody!.categoryBitMask = GameConstants.PhysicsCategories.miscCategory
            sprite.physicsBody!.contactTestBitMask = GameConstants.PhysicsCategories.obstacleCategory
            sprite.physicsBody!.collisionBitMask = GameConstants.PhysicsCategories.noCategory
        default:
            sprite.physicsBody = SKPhysicsBody(rectangleOf: sprite.size)
            sprite.physicsBody!.affectedByGravity = false
            sprite.physicsBody!.allowsRotation = false
            sprite.physicsBody!.isDynamic = false
            sprite.physicsBody!.restitution = 0.0
            sprite.physicsBody!.collisionBitMask = GameConstants.PhysicsCategories.allCategory
        }
    }
    
    static func addBossPhysics(_ sprite: SKSpriteNode) {
        let value = sprite.userData!["type"] as! Int
        let type = GameConstants.BossTypes.init(rawValue: value)
        
        switch type {
        case .RobotArm:
            sprite.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: sprite.size.width, height: sprite.size.height*0.7), center: CGPoint(x: sprite.anchorPoint.x, y: sprite.anchorPoint.y - 100))
            sprite.physicsBody!.affectedByGravity = false
            sprite.physicsBody!.restitution = 0.0
            sprite.physicsBody!.categoryBitMask = GameConstants.PhysicsCategories.enemyCategory
            sprite.physicsBody!.collisionBitMask = GameConstants.PhysicsCategories.groundCategory
            sprite.physicsBody!.contactTestBitMask = GameConstants.PhysicsCategories.allCategory
        case .Suction:
            let child = sprite.childNode(withName: "Suction-Head") as! SKSpriteNode
            child.physicsBody = SKPhysicsBody(rectangleOf: child.size)
            child.physicsBody!.affectedByGravity = false
            child.physicsBody!.restitution = 0.0
            child.physicsBody!.categoryBitMask = GameConstants.PhysicsCategories.enemyCategory
            child.physicsBody!.collisionBitMask = GameConstants.PhysicsCategories.noCategory
            child.physicsBody!.contactTestBitMask = GameConstants.PhysicsCategories.playerCategory
        default:
            break
        }
    }
    
    static func getArcPhysics(_ sprite: SKSpriteNode, startRadian: CGFloat, endRadian: CGFloat, arcCenterDir: CGPoint, clockwise: Bool, duration: Double) -> SKAction {
        var action: SKAction
        var arcRadius: CGFloat
        // Fix position based of frame size
        if let radius = sprite.userData?.value(forKey: "arcRadius") as? CGFloat {
            arcRadius = radius
        } else {
            debugPrint("arcRadius not set on " + sprite.name! )
            return SKAction.run {}
        }
        let center = CGPoint(x: sprite.position.x + (arcCenterDir.x * arcRadius), y: sprite.position.y + (arcCenterDir.y * arcRadius))
        let path = UIBezierPath(arcCenter: center, radius: arcRadius, startAngle: startRadian, endAngle: endRadian, clockwise: clockwise) // clockwise works in reverse
        
        action = SKAction.repeatForever(.follow(path.cgPath, asOffset: false, orientToPath: true, duration: duration))
        return action
    }
    
    static func addSpikesPhysics(_ sprite: SKSpriteNode) {
        sprite.physicsBody = SKPhysicsBody(texture: sprite.texture!, size: sprite.size)
        sprite.physicsBody!.affectedByGravity = false
        sprite.physicsBody!.allowsRotation = false
        sprite.physicsBody!.categoryBitMask = GameConstants.PhysicsCategories.obstacleCategory
        sprite.physicsBody!.collisionBitMask = GameConstants.PhysicsCategories.noCategory
        sprite.physicsBody!.contactTestBitMask =
            GameConstants.PhysicsCategories.playerCategory
    }
    
    static func addTrapPhysics(_ sprite: SKSpriteNode, _ maxXY: CGVector) {
        sprite.physicsBody = SKPhysicsBody(rectangleOf: sprite.size)
        sprite.physicsBody!.affectedByGravity = false
        sprite.physicsBody!.allowsRotation = false
        sprite.physicsBody!.categoryBitMask = GameConstants.PhysicsCategories.trapCategory
        sprite.physicsBody!.collisionBitMask = GameConstants.PhysicsCategories.noCategory
        sprite.physicsBody!.contactTestBitMask =
            GameConstants.PhysicsCategories.playerCategory | GameConstants.PhysicsCategories.groundCategory
        
        if let tileValue = sprite.userData?.value(forKey: "flame") {
            // flame animation
            let idleFrames = AnimationHelper.loadTextures(from: SKTextureAtlas(named: GameConstants.StringConstants.FlameIdleAtlas), withName: GameConstants.StringConstants.FlameidlePrefixKey)
            let enumMoveType = GameConstants.flameMoveType.init(rawValue: tileValue as! GameConstants.flameMoveType.RawValue)! as GameConstants.flameMoveType
            var moveAction: SKAction = .changeMass(by: 1, duration: 1)
            var duration: Double
            if let val = sprite.userData?.value(forKey: "duration") as? Double {
                duration = val
            } else {
                duration = 2
            }
            //let rand = 1.2//Double(arc4random_uniform(9))/10.0
            let toZeroY = SKAction.moveTo(y: 0, duration: duration)
            let toMaxY = SKAction.moveTo(y: maxXY.dy, duration: duration)
            let toPrevY = SKAction.moveTo(y: sprite.position.y, duration: duration)
            let toZeroX = SKAction.moveTo(x: 0, duration: duration)
            let toMaxX = SKAction.moveTo(x: maxXY.dx, duration: duration)
            let toPrevX = SKAction.moveTo(x: sprite.position.x, duration: duration)
            let hide = SKAction.hide()
            let reveal = SKAction.unhide()
            var startRadian: CGFloat
            var endRadian: CGFloat
            let animation = SKAction.repeatForever(SKAction.animate(with: idleFrames, timePerFrame: 0.08, resize: true, restore: true))
            switch(enumMoveType) {
            case .UpDown:
                moveAction = SKAction.repeatForever(SKAction.sequence([SKAction.group([reveal, toZeroY]), SKAction.group([hide, toPrevY])]))
                break
            case .DownUp:
                moveAction = SKAction.repeatForever(SKAction.sequence([SKAction.group([reveal, toMaxY]), SKAction.group([hide, toPrevY])]))
                break
            case .LeftRight:
                moveAction = SKAction.repeatForever(SKAction.sequence([SKAction.group([reveal, toMaxX]), SKAction.group([hide, toPrevX])]))
                break
            case .RightLeft:
                moveAction = SKAction.repeatForever(SKAction.sequence([SKAction.group([reveal, toZeroX]), SKAction.group([hide, toPrevX])]))
                break
            case .curveBotLeft:
                sprite.yScale = -1
                startRadian = CGFloat(320.0 * Float.pi/180) // 360 - 40
                endRadian = CGFloat(220.0 * Float.pi/180) // 180 + 40
                let centerOffset = CGPoint(x: -1, y: 0)
                moveAction = getArcPhysics(sprite, startRadian: startRadian, endRadian: endRadian, arcCenterDir: centerOffset, clockwise: true, duration: duration)
                break
            case .curveBotRight:
                sprite.yScale = -1
                startRadian = CGFloat(220.0 * Float.pi/180)
                endRadian = CGFloat(320.0 * Float.pi/180)
                let centerOffset = CGPoint(x: 1, y: 0)
                moveAction = getArcPhysics(sprite, startRadian: startRadian, endRadian: endRadian, arcCenterDir: centerOffset, clockwise: false, duration: duration)
                break
            case .curveTopLeft:
                sprite.yScale = -1
                startRadian = CGFloat(40.0 * Float.pi/180)
                endRadian = CGFloat(140.0 * Float.pi/180)
                let centerOffset = CGPoint(x: -1, y: 0)
                moveAction = getArcPhysics(sprite, startRadian: startRadian, endRadian: endRadian, arcCenterDir: centerOffset, clockwise: false, duration: duration)
                break
            case .curveTopRight:
                sprite.yScale = -1
                startRadian = CGFloat(140.0 * Float.pi/180)
                endRadian = CGFloat(40.0 * Float.pi/180)
                let centerOffset = CGPoint(x: 1, y: 0)
                moveAction = getArcPhysics(sprite, startRadian: startRadian, endRadian: endRadian, arcCenterDir: centerOffset, clockwise: true, duration: duration)
                break
            case .curveLeftUp:
                sprite.yScale = -1  // For making sure the flame image is facing in right direction while animating
                startRadian = CGFloat(230.0 * Float.pi/180)
                endRadian = CGFloat(130.0 * Float.pi/180)
                let centerOffset = CGPoint(x: 0, y: 1)
                moveAction = getArcPhysics(sprite, startRadian: startRadian, endRadian: endRadian, arcCenterDir: centerOffset, clockwise: true, duration: duration)
                break
            case .curveLeftDown:
                sprite.yScale = -1  // For making sure the flame image is facing in right direction while animating
                startRadian = CGFloat(130.0 * Float.pi/180)
                endRadian = CGFloat(230.0 * Float.pi/180)
                let centerOffset = CGPoint(x: 0, y:-1)
                moveAction = getArcPhysics(sprite, startRadian: startRadian, endRadian: endRadian, arcCenterDir: centerOffset, clockwise: false, duration: duration)
                break
            case .curveRightUp:
                startRadian = CGFloat(310.0 * Float.pi/180)
                endRadian = CGFloat(50.0 * Float.pi/180)
                let centerOffset = CGPoint(x: 0, y: 1)
                moveAction = getArcPhysics(sprite, startRadian: startRadian, endRadian: endRadian, arcCenterDir: centerOffset, clockwise: false, duration: duration)
                break
            case .curveRightDown:
                startRadian = CGFloat(50.0 * Float.pi/180)
                endRadian = CGFloat(310.0 * Float.pi/180)
                let centerOffset = CGPoint(x: 0, y: -1)
                moveAction = getArcPhysics(sprite, startRadian: startRadian, endRadian: endRadian, arcCenterDir: centerOffset, clockwise: true, duration: duration)
                break
            default:
                moveAction = SKAction.changeMass(by: 1, duration: 1)
                break
            }
            sprite.run(SKAction.group([moveAction, animation]))
        } else {
            // Static Traps
        }
    }
    
    static func addGroundPhysics( tileMapNode: SKTileMapNode, with info: String) {
        let tileSize = tileMapNode.tileSize
        // Top and bottom physics can be set per row horizontally
        for row in 0..<tileMapNode.numberOfRows {
            var tilesTop:[Int] = []
            var tilesBottom:[Int] = []
            var tilesSlopeL:[Int] = []
            var tilesSlopeR:[Int] = []
            var tilesSlopeLH:[Int] = []
            var tilesSlopeLL:[Int] = []
            var tilesSlopeRH:[Int] = []
            var tilesSlopeRL:[Int] = []
            var tilesSlopeLB:[Int] = []
            var tilesSlopeRB:[Int] = []
            var tilesSlopeLBH:[Int] = []
            var tilesSlopeLBL:[Int] = []
            var tilesSlopeRBH:[Int] = []
            var tilesSlopeRBL:[Int] = []
            
            for col in 0..<tileMapNode.numberOfColumns {
                let tileDefinition = tileMapNode.tileDefinition(atColumn: col, row: row)
                var infoValue = tileDefinition?.userData?.value(forKey: "top") as? Bool
                tilesTop.append(infoValue == true ? 1:0)
                infoValue = tileDefinition?.userData?.value(forKey: "bottom") as? Bool
                tilesBottom.append(infoValue == true ? 1:0)
                infoValue = tileDefinition?.userData?.value(forKey: "SlopeL") as? Bool
                tilesSlopeL.append(infoValue == true ? 1:0)
                infoValue = tileDefinition?.userData?.value(forKey: "SlopeR") as? Bool
                tilesSlopeR.append(infoValue == true ? 1:0)
                infoValue = tileDefinition?.userData?.value(forKey: "SlopeLH") as? Bool
                tilesSlopeLH.append(infoValue == true ? 1:0)
                infoValue = tileDefinition?.userData?.value(forKey: "SlopeLL") as? Bool
                tilesSlopeLL.append(infoValue == true ? 1:0)
                infoValue = tileDefinition?.userData?.value(forKey: "SlopeRH") as? Bool
                tilesSlopeRH.append(infoValue == true ? 1:0)
                infoValue = tileDefinition?.userData?.value(forKey: "SlopeRL") as? Bool
                tilesSlopeRL.append(infoValue == true ? 1:0)
                infoValue = tileDefinition?.userData?.value(forKey: "SlopeLB") as? Bool
                tilesSlopeLB.append(infoValue == true ? 1:0)
                infoValue = tileDefinition?.userData?.value(forKey: "SlopeRB") as? Bool
                tilesSlopeRB.append(infoValue == true ? 1:0)
                infoValue = tileDefinition?.userData?.value(forKey: "SlopeLBH") as? Bool
                tilesSlopeLBH.append(infoValue == true ? 1:0)
                infoValue = tileDefinition?.userData?.value(forKey: "SlopeLBL") as? Bool
                tilesSlopeLBL.append(infoValue == true ? 1:0)
                infoValue = tileDefinition?.userData?.value(forKey: "SlopeRBH") as? Bool
                tilesSlopeRBH.append(infoValue == true ? 1:0)
                infoValue = tileDefinition?.userData?.value(forKey: "SlopeRBL") as? Bool
                tilesSlopeRBL.append(infoValue == true ? 1:0)
                
                
                // taking care of physics body on horizontal surfaces
                if tilesTop.contains(1) {
                    var platform = [Int]()
                    for (index, tile) in tilesTop.enumerated() {
                        if(tile == 1 && index < (tileMapNode.numberOfColumns - 1) ) {
                            platform.append(index)
                        } else if !platform.isEmpty {
                            let x = (CGFloat)(platform[0]) * tileSize.width
                            let y = (CGFloat)(row) * tileSize.height
                            let tileNode = GroundNode(with: CGSize(width: CGFloat(platform.count) *
                                                                    tileSize.width, height: tileSize.height), type: GameConstants.TileType.horizontal)
                            tileNode.position = CGPoint(x: x, y: y)
                            tileNode.anchorPoint = CGPoint.zero
                            tileMapNode.addChild(tileNode)
                            platform.removeAll()
                        }
                    }
                }
                if tilesBottom.contains(1) {
                    var platform = [Int]()
                    for (index, tile) in tilesBottom.enumerated() {
                        if(tile == 1 && index < (tileMapNode.numberOfColumns - 1) ) {
                            platform.append(index)
                        } else if !platform.isEmpty {
                            let x = (CGFloat)(platform[0]) * tileSize.width
                            let y = (CGFloat)(row) * tileSize.height
                            let tileNode = GroundNode(with: CGSize(width: CGFloat(platform.count) *
                                                                    tileSize.width, height: 0), type: GameConstants.TileType.horizontal)
                            tileNode.position = CGPoint(x: x, y: y)
                            tileNode.anchorPoint = CGPoint.zero
                            tileMapNode.addChild(tileNode)
                            platform.removeAll()
                        }
                    }
                }
                if tilesSlopeL.contains(1) {
                    var platform = [Int]()
                    for (index, tile) in tilesSlopeL.enumerated() {
                        if(tile == 1 && index < (tileMapNode.numberOfColumns - 1) ) {
                            platform.append(index)
                        } else if !platform.isEmpty {
                            let x = (CGFloat)(platform[0]) * tileSize.width
                            let y = (CGFloat)(row) * tileSize.height
                            let tileNode = GroundNode(with: CGSize(width: CGFloat(platform.count) *
                                                                    tileSize.width, height: CGFloat(platform.count) * tileSize.height), type: GameConstants.TileType.SlopeL)
                            tileNode.position = CGPoint(x: x, y: y)
                            tileNode.anchorPoint = CGPoint.zero
                            tileMapNode.addChild(tileNode)
                            platform.removeAll()
                        }
                    }
                }
                if tilesSlopeR.contains(1) {
                    var platform = [Int]()
                    for (index, tile) in tilesSlopeR.enumerated() {
                        if(tile == 1 && index < (tileMapNode.numberOfColumns - 1) ) {
                            platform.append(index)
                        } else if !platform.isEmpty {
                            let x = (CGFloat)(platform[0]) * tileSize.width
                            let y = (CGFloat)(row) * tileSize.height
                            let tileNode = GroundNode(with: CGSize(width: CGFloat(platform.count) *
                                                                    tileSize.width, height: CGFloat(platform.count) * tileSize.height), type: GameConstants.TileType.SlopeR)
                            tileNode.position = CGPoint(x: x, y: y)
                            tileNode.anchorPoint = CGPoint.zero
                            tileMapNode.addChild(tileNode)
                            platform.removeAll()
                        }
                    }
                }
                if tilesSlopeLH.contains(1) {
                    var platform = [Int]()
                    for (index, tile) in tilesSlopeLH.enumerated() {
                        if(tile == 1 && index < (tileMapNode.numberOfColumns - 1) ) {
                            platform.append(index)
                        } else if !platform.isEmpty {
                            let x = (CGFloat)(platform[0]) * tileSize.width
                            let y = (CGFloat)(row) * tileSize.height
                            let tileNode = GroundNode(with: CGSize(width: CGFloat(platform.count) *
                                                                    tileSize.width, height: CGFloat(platform.count) * tileSize.height), type: GameConstants.TileType.SlopeLH)
                            tileNode.position = CGPoint(x: x, y: y)
                            tileNode.anchorPoint = CGPoint.zero
                            tileMapNode.addChild(tileNode)
                            platform.removeAll()
                        }
                    }
                }
                if tilesSlopeLL.contains(1) {
                    var platform = [Int]()
                    for (index, tile) in tilesSlopeLL.enumerated() {
                        if(tile == 1 && index < (tileMapNode.numberOfColumns - 1) ) {
                            platform.append(index)
                        } else if !platform.isEmpty {
                            let x = (CGFloat)(platform[0]) * tileSize.width
                            let y = (CGFloat)(row) * tileSize.height
                            let tileNode = GroundNode(with: CGSize(width: CGFloat(platform.count) *
                                                                    tileSize.width, height: CGFloat(platform.count) * tileSize.height), type: GameConstants.TileType.SlopeLL)
                            tileNode.position = CGPoint(x: x, y: y)
                            tileNode.anchorPoint = CGPoint.zero
                            tileMapNode.addChild(tileNode)
                            platform.removeAll()
                        }
                    }
                }
                if tilesSlopeRH.contains(1) {
                    var platform = [Int]()
                    for (index, tile) in tilesSlopeRH.enumerated() {
                        if(tile == 1 && index < (tileMapNode.numberOfColumns - 1) ) {
                            platform.append(index)
                        } else if !platform.isEmpty {
                            let x = (CGFloat)(platform[0]) * tileSize.width
                            let y = (CGFloat)(row) * tileSize.height
                            let tileNode = GroundNode(with: CGSize(width: CGFloat(platform.count) *
                                                                    tileSize.width, height: CGFloat(platform.count) * tileSize.height), type: GameConstants.TileType.SlopeRH)
                            tileNode.position = CGPoint(x: x, y: y)
                            tileNode.anchorPoint = CGPoint.zero
                            tileMapNode.addChild(tileNode)
                            platform.removeAll()
                        }
                    }
                }
                if tilesSlopeRL.contains(1) {
                    var platform = [Int]()
                    for (index, tile) in tilesSlopeRL.enumerated() {
                        if(tile == 1 && index < (tileMapNode.numberOfColumns - 1) ) {
                            platform.append(index)
                        } else if !platform.isEmpty {
                            let x = (CGFloat)(platform[0]) * tileSize.width
                            let y = (CGFloat)(row) * tileSize.height
                            let tileNode = GroundNode(with: CGSize(width: CGFloat(platform.count) *
                                                                    tileSize.width, height: CGFloat(platform.count) * tileSize.height), type: GameConstants.TileType.SlopeRL)
                            tileNode.position = CGPoint(x: x, y: y)
                            tileNode.anchorPoint = CGPoint.zero
                            tileMapNode.addChild(tileNode)
                            platform.removeAll()
                        }
                    }
                }
                if tilesSlopeLB.contains(1) {
                    var platform = [Int]()
                    for (index, tile) in tilesSlopeLB.enumerated() {
                        if(tile == 1 && index < (tileMapNode.numberOfColumns - 1) ) {
                            platform.append(index)
                        } else if !platform.isEmpty {
                            let x = (CGFloat)(platform[0]) * tileSize.width
                            let y = (CGFloat)(row) * tileSize.height
                            let tileNode = GroundNode(with: CGSize(width: CGFloat(platform.count) *
                                                                    tileSize.width, height: CGFloat(platform.count) * tileSize.height), type: GameConstants.TileType.SlopeLB)
                            tileNode.position = CGPoint(x: x, y: y)
                            tileNode.anchorPoint = CGPoint.zero
                            tileMapNode.addChild(tileNode)
                            platform.removeAll()
                        }
                    }
                }
                if tilesSlopeRB.contains(1) {
                    var platform = [Int]()
                    for (index, tile) in tilesSlopeRB.enumerated() {
                        if(tile == 1 && index < (tileMapNode.numberOfColumns - 1) ) {
                            platform.append(index)
                        } else if !platform.isEmpty {
                            let x = (CGFloat)(platform[0]) * tileSize.width
                            let y = (CGFloat)(row) * tileSize.height
                            let tileNode = GroundNode(with: CGSize(width: CGFloat(platform.count) *
                                                                    tileSize.width, height: CGFloat(platform.count) * tileSize.height), type: GameConstants.TileType.SlopeRB)
                            tileNode.position = CGPoint(x: x, y: y)
                            tileNode.anchorPoint = CGPoint.zero
                            tileMapNode.addChild(tileNode)
                            platform.removeAll()
                        }
                    }
                }
                if tilesSlopeLBH.contains(1) {
                    var platform = [Int]()
                    for (index, tile) in tilesSlopeLBH.enumerated() {
                        if(tile == 1 && index < (tileMapNode.numberOfColumns - 1) ) {
                            platform.append(index)
                        } else if !platform.isEmpty {
                            let x = (CGFloat)(platform[0]) * tileSize.width
                            let y = (CGFloat)(row) * tileSize.height
                            let tileNode = GroundNode(with: CGSize(width: CGFloat(platform.count) *
                                                                    tileSize.width, height: CGFloat(platform.count) * tileSize.height), type: GameConstants.TileType.SlopeLBH)
                            tileNode.position = CGPoint(x: x, y: y)
                            tileNode.anchorPoint = CGPoint.zero
                            tileMapNode.addChild(tileNode)
                            platform.removeAll()
                        }
                    }
                }
                if tilesSlopeLBL.contains(1) {
                    var platform = [Int]()
                    for (index, tile) in tilesSlopeLBL.enumerated() {
                        if(tile == 1 && index < (tileMapNode.numberOfColumns - 1) ) {
                            platform.append(index)
                        } else if !platform.isEmpty {
                            let x = (CGFloat)(platform[0]) * tileSize.width
                            let y = (CGFloat)(row) * tileSize.height
                            let tileNode = GroundNode(with: CGSize(width: CGFloat(platform.count) *
                                                                    tileSize.width, height: CGFloat(platform.count) * tileSize.height), type: GameConstants.TileType.SlopeLBL)
                            tileNode.position = CGPoint(x: x, y: y)
                            tileNode.anchorPoint = CGPoint.zero
                            tileMapNode.addChild(tileNode)
                            platform.removeAll()
                        }
                    }
                }
                if tilesSlopeRBH.contains(1) {
                    var platform = [Int]()
                    for (index, tile) in tilesSlopeRBH.enumerated() {
                        if(tile == 1 && index < (tileMapNode.numberOfColumns - 1) ) {
                            platform.append(index)
                        } else if !platform.isEmpty {
                            let x = (CGFloat)(platform[0]) * tileSize.width
                            let y = (CGFloat)(row) * tileSize.height
                            let tileNode = GroundNode(with: CGSize(width: CGFloat(platform.count) *
                                                                    tileSize.width, height: CGFloat(platform.count) * tileSize.height), type: GameConstants.TileType.SlopeRBH)
                            tileNode.position = CGPoint(x: x, y: y)
                            tileNode.anchorPoint = CGPoint.zero
                            tileMapNode.addChild(tileNode)
                            platform.removeAll()
                        }
                    }
                }
                if tilesSlopeRBL.contains(1) {
                    var platform = [Int]()
                    for (index, tile) in tilesSlopeRBL.enumerated() {
                        if(tile == 1 && index < (tileMapNode.numberOfColumns - 1) ) {
                            platform.append(index)
                        } else if !platform.isEmpty {
                            let x = (CGFloat)(platform[0]) * tileSize.width
                            let y = (CGFloat)(row) * tileSize.height
                            let tileNode = GroundNode(with: CGSize(width: CGFloat(platform.count) *
                                                                    tileSize.width, height: CGFloat(platform.count) * tileSize.height), type: GameConstants.TileType.SlopeRBL)
                            tileNode.position = CGPoint(x: x, y: y)
                            tileNode.anchorPoint = CGPoint.zero
                            tileMapNode.addChild(tileNode)
                            platform.removeAll()
                        }
                    }
                }
            }
        }
        // Left and right physics can be set per row vertically
        for col in 0..<tileMapNode.numberOfColumns {
            var tilesLeft:[Int] = []
            var tilesRight:[Int] = []
            
            for row in 0..<tileMapNode.numberOfRows {
                let tileDefinition = tileMapNode.tileDefinition(atColumn: col, row: row)
                var infoValue = tileDefinition?.userData?.value(forKey: "left") as? Bool
                tilesLeft.append(infoValue == true ? 1:0)
                infoValue = tileDefinition?.userData?.value(forKey: "right") as? Bool
                tilesRight.append(infoValue == true ? 1:0)
                
                // taking care of physics body on vertical surfaces
                if tilesLeft.contains(1) {
                    var platform = [Int]()
                    for (index, tile) in tilesLeft.enumerated() {
                        if(tile == 1 && index < (tileMapNode.numberOfRows - 1) ) {
                            platform.append(index)
                        } else if !platform.isEmpty {
                            let y = (CGFloat)(platform[0]) * tileSize.width
                            let x = (CGFloat)(col) * tileSize.height
                            let tileNode = GroundNode(with: CGSize(width:
                                                                    0, height: CGFloat(platform.count) * tileSize.height), type:GameConstants.TileType.vertical)
                            tileNode.position = CGPoint(x: x, y: y)
                            tileNode.anchorPoint = CGPoint.zero
                            tileMapNode.addChild(tileNode)
                            platform.removeAll()
                        }
                    }
                }
                if tilesRight.contains(1) {
                    var platform = [Int]()
                    for (index, tile) in tilesRight.enumerated() {
                        if(tile == 1 && index < (tileMapNode.numberOfRows - 1) ) {
                            platform.append(index)
                        } else if !platform.isEmpty {
                            let y = (CGFloat)(platform[0]) * tileSize.width
                            let x = (CGFloat)(col) * tileSize.height
                            let tileNode = GroundNode(with: CGSize(width:
                                tileSize.width, height: CGFloat(platform.count) *
                                    tileSize.height), type:GameConstants.TileType.vertical)
                            tileNode.position = CGPoint(x: x, y: y)
                            tileNode.anchorPoint = CGPoint.zero
                            tileMapNode.addChild(tileNode)
                            platform.removeAll()
                        }
                    }
                }
            }
        }
        
    }
}
