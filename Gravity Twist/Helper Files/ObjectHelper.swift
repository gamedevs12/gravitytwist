//
//  ObjectHelper.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 2/8/18.
//  Copyright © 2018 Narv. All rights reserved.
//

import SpriteKit

class ObjectHelper {
    
    static func handleChild(_ sprite: SKSpriteNode, _ maxXY: CGVector) {
        switch sprite.name! {
        case GameConstants.StringConstants.trapName:
            PhysicsHelper.addTrapPhysics(sprite, maxXY)
        case GameConstants.StringConstants.spikesGroupName, GameConstants.StringConstants.rockGroupName:
            for i in sprite.children {
                handleChild(i as! SKSpriteNode, maxXY)
            }
        case GameConstants.StringConstants.spikesName:
            PhysicsHelper.addSpikesPhysics(sprite)
        case GameConstants.StringConstants.coinName, GameConstants.StringConstants.doorName,
             GameConstants.StringConstants.enemyName, GameConstants.StringConstants.gravityField,
             GameConstants.StringConstants.bossName, GameConstants.StringConstants.bossRageName, GameConstants.StringConstants.rockSpawnName, GameConstants.StringConstants.colliderName,
             GameConstants.StringConstants.nonTilePlatformName, GameConstants.StringConstants.leverName, GameConstants.StringConstants.rockName, GameConstants.StringConstants.movingPlatformName:
            PhysicsHelper.addPhysics(sprite)
        default:
            break
        }
    }
    
    static func addAction(_ sprite: SKSpriteNode, atlas: SKTextureAtlas) {
        var atlasName : String = ""
        switch sprite.name! {
        case GameConstants.StringConstants.coinName:
            atlasName = "coin"
        case GameConstants.StringConstants.gravityField:
            atlasName = "field"
        default:
            break
        }
        sprite.color = UIColor.blue
        let frames = AnimationHelper.loadTextures(from: atlas, withName: atlasName)
        sprite.run(SKAction.repeatForever(SKAction.animate(with: frames, timePerFrame: GameConstants.TextureConstants.defaultAnimationSpeed, resize: false, restore: false)))
    }
    
    static func addAction(_ sprite: SKSpriteNode, frames: [SKTexture]) {
        sprite.run(SKAction.repeatForever(SKAction.animate(with: frames, timePerFrame: GameConstants.TextureConstants.defaultAnimationSpeed, resize: false, restore: false)))
    }
    
    static func textureReplace(_ sprite: SKSpriteNode, texture: SKTexture) {
        sprite.texture = texture
    }
    
    static func extractLevelItemDialogues(world: Int, level: Int) -> [String] {
        if let dialogues = GameConstants.DialogueSequences.levelItemDialogueMap[GameConstants.Point(x: world, y: level)] {
            return dialogues
        }
        return [String()]
    }
    
    static func extractLevelDialogues(world: Int, level: Int) -> [String] {
        if let dialogues = GameConstants.DialogueSequences.levelDiagMap[GameConstants.Point(x: world, y: level)] {
            return dialogues
        }
        return [String()]
    }
}
