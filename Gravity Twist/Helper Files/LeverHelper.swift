//
//  LeverHelper.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 4/14/21.
//  Copyright © 2021 Narv. All rights reserved.
//

import SpriteKit

class LeverHelper {

    var world: Int = 0
    var level: Int = 0
    var isStopping = false
    var isLevelShiftRunning = false
    var groupRunning = -1
    var levelShiftMap: [Int: [CGPoint]]
    var duration: Double
    var currentGroup: Int
    
    init(_ world: Int, level: Int) {
        self.world = world
        self.level = level
        self.isStopping = false
        self.isLevelShiftRunning = false
        self.levelShiftMap = [Int: [CGPoint]]()
        if let data = GameConstants.LevelConstants.levelShiftMap[GameConstants.Point(x: world, y: level)] {
            self.levelShiftMap = data
        }
        self.duration = GameConstants.LevelConstants.defaultActionDuration
        if let d = GameConstants.LevelConstants.levelShiftDurationMap[GameConstants.Point(x: world, y: level)] {
            self.duration = d
        }
        self.currentGroup = 0
    }
    
    func runDefaultAction(_ tileMap: SKTileMapNode) {
        var actionList = [SKAction]()
        guard let _ = levelShiftMap[currentGroup] else {
            return
        }
        for coord in levelShiftMap[currentGroup]! {
            let moveUpAction = SKAction.moveBy(x: coord.x, y: coord.y, duration: duration)
            let waitAction = SKAction.wait(forDuration: duration)
            let moveDownAction = SKAction.moveBy(x: -coord.x, y: -coord.y, duration: duration)
            let levelShiftDone = SKAction.run {
                if self.isStopping {
                    self.stopLevelShift(tileMap)
                }
            }
            actionList.append(moveUpAction)
            actionList.append(waitAction)
            actionList.append(moveDownAction)
            actionList.append(levelShiftDone)
        }
        tileMap.run(SKAction.repeatForever(SKAction.sequence(actionList)))
        isLevelShiftRunning = true
    }
    
    func getAction(_ point: CGPoint) -> SKAction {
        return SKAction.moveBy(x: point.x, y: point.y, duration: duration)
    }
    
    func startLevelShift(_ tileMap: SKTileMapNode)
    {
        let levelRef = GameConstants.Point(x: world, y: level)
        guard let _ = GameConstants.LevelConstants.leverActionMap[levelRef] else {
            if !isLevelShiftRunning {
                runDefaultAction(tileMap)
                isLevelShiftRunning = true
            }
            return
        }
        var actionList = [SKAction]()
        for cIdx in 0..<levelShiftMap[currentGroup]!.count {
            actionList.append(getAction(levelShiftMap[currentGroup]![cIdx]))
        }
        let levelShiftDone = SKAction.run {
            //if self.isStopping {
                self.stopLevelShift(tileMap)
            //}
        }
        actionList.append(levelShiftDone)
        // TODO:
        // Lever variables: Coords, Repeat?, isRunning
        // repeatforever should be added in gameconstants
        // should ignore current running variable
        isLevelShiftRunning = true
        tileMap.run(SKAction.sequence(actionList))
    }

    func stopLevelShift(_ tileMap: SKTileMapNode) {
        tileMap.removeAllActions()
        isStopping = false
        isLevelShiftRunning = false
    }

    func switchLeverState(_ lever: SKSpriteNode) -> Int {
        var mode = 1
        if let value = lever.userData?["state"] as? Int {
            if value == 1 {
                lever.userData?.setValue(0, forKey: "state")
                isLevelShiftRunning = false
                mode = 0
            } else {
                lever.userData?.setValue(1, forKey: "state")
                self.startLevelShift(lever.parent as! SKTileMapNode)
                if lever.children.count > 0 {
                    groupRunning = Int(lever.children[0].name!)!
                }
            }
        } else {
            lever.userData = NSMutableDictionary()
            var leverGroup = -2
            if lever.children.count > 0 {
                leverGroup = Int(lever.children[0].name!)!
            }
            if isLevelShiftRunning && groupRunning != leverGroup {
                lever.userData?.setValue(0, forKey: "state")
                mode = 0
            } else {
                lever.userData?.setValue(1, forKey: "state")
                self.startLevelShift(lever.parent as! SKTileMapNode)
                if lever.children.count > 0 {
                    groupRunning = Int(lever.children[0].name!)!
                }
            }
        }
        return mode
    }

    func animateAllLevers(_ levers: [SKSpriteNode], spriteSheet: SpriteSheet) {
        for node in levers {
            var frames: [SKTexture] = spriteSheet.animatedObjects[GameConstants.ObjectTypes.leveron]!
            
            if self.switchLeverState(node) == 0 {
                isStopping = true
                frames = spriteSheet.animatedObjects[GameConstants.ObjectTypes.leveroff]!
            }
            node.run(SKAction.animate(with: frames, timePerFrame: GameConstants.TextureConstants.defaultAnimationSpeed, resize: false, restore: false))

            let levelRef = GameConstants.Point(x: world, y: level)
            if groupRunning != -1 && GameConstants.LevelConstants.levelShiftOnceMap.contains(levelRef) {
                isStopping = false
            }
        }
    }
}
