//
//  AudioHelper.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 6/19/21.
//  Copyright © 2021 Narv. All rights reserved.
//

import AVFoundation

struct Audio {
    static var audioPlayer = AVAudioPlayer()
    static var musicTypePlaying = GameConstants.MusicType.menu
    static var volumeFloat: Float = 0.5
    
    static func changeToMenuMusic() {
        if audioPlayer.isPlaying && musicTypePlaying == .menu {
            return
        }
        changeMusic(to: Bundle.main.url(forResource: GameConstants.StringConstants.menuMusicName, withExtension: "mp3")!)
        musicTypePlaying = .menu
    }
    
    static func changeToGamePlayMusic() {
        if audioPlayer.isPlaying && musicTypePlaying == .gameplay {
            return
        }
        changeMusic(to: Bundle.main.url(forResource: GameConstants.StringConstants.gameplayMusicName, withExtension: "mp3")!)
        musicTypePlaying = .gameplay
    }
    
    static func changeToBossMusic() {
        if audioPlayer.isPlaying && musicTypePlaying == .boss {
            return
        }
        changeMusic(to: Bundle.main.url(forResource: GameConstants.StringConstants.bossMusicName, withExtension: "mp3")!)
        musicTypePlaying = .boss
    }
    
    static func changeToVictoryMusic() {
        if audioPlayer.isPlaying && musicTypePlaying == .victory {
            return
        }
        changeMusic(to: Bundle.main.url(forResource: GameConstants.StringConstants.victoryMusicName, withExtension: "mp3")!)
        musicTypePlaying = .victory
    }
    
    static func changeMusic(to audioUrl: URL) {
        if audioPlayer.isPlaying {
            audioPlayer.stop()
        }
        do {
            audioPlayer = try AVAudioPlayer.init(contentsOf: audioUrl)
            audioPlayer.play()
            audioPlayer.numberOfLoops = -1
        } catch {
            debugPrint("audio init error")
        }
        audioPlayer.volume = volumeFloat
    }
    
    static func stopMusic() {
        if audioPlayer.isPlaying {
            audioPlayer.stop()
        }
    }
}
