//
//  GameViewController.swift
//  Gravity Twist
//
//  Created by Abhinav Rathod on 1/30/18.
//  Copyright © 2018 Narv. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import AVFoundation

class GameViewController: UIViewController {

    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        presentMenuScene()
        //presentGameScene(for: 0, in: 4)
        //presentTransitionScene(nextLevel: 5, nextWorld: 1)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var preferredScreenEdgesDeferringSystemGestures: UIRectEdge {
        return UIRectEdge.all
    }
}

extension GameViewController: SceneManagerDelegate {
    
    func presentLevelScene(for level: Int, in world: Int) {
        let idx = (level-1)/GameConstants.LevelConstants.levelSubcount
        let scene = LevelSelectScene(size: view.bounds.size, world: world, idx: Int(idx), delegate: self)
        scene.scaleMode = .aspectFit
        present(scene: scene)
    }
    
    func presentWorldScene() {
        let playScreen: CGSize = view.safeAreaLayoutGuide.layoutFrame.size
        let scene = WorldSelectScene(size: playScreen, startIndex: 0, sceneManagerDelegate: self)
        scene.scaleMode = .aspectFit
        present(scene: scene)
    }
    
    func presentGameScene(for world: Int, in level: Int) {
        if(world > 2) {
            debugPrint("No Scenes Setup yet!!")
            return
        }
        let playScreen: CGSize = view.safeAreaLayoutGuide.layoutFrame.size
        
        let scene = GameScene(size: CGSize(width: playScreen.width, height: playScreen.height), world: world, level: level, sceneManagerDelegate: self)
        scene.scaleMode = .aspectFit
        present(scene: scene)
    }
    
    func presentIntroScene() {
        let playScreen: CGSize = view.safeAreaLayoutGuide.layoutFrame.size
        let scene = IntroScene(size: playScreen, sceneManagerDelegate: self)
        scene.scaleMode = .aspectFit
        present(scene: scene)
    }
    
    func presentMenuScene() {
        let playScreen: CGSize = view.safeAreaLayoutGuide.layoutFrame.size
        let scene = MenuScene(size: playScreen, sceneManagerDelegate: self)
        scene.scaleMode = .aspectFit
        present(scene: scene)
    }
    
    func present(scene: SKScene) {
        if let view = self.view as! SKView? {
            view.presentScene(scene, transition: .fade(withDuration: 1))
            view.ignoresSiblingOrder = true
            view.showsPhysics = false
            view.showsFPS = false
            view.showsNodeCount = false
            if let gameScene = scene as? GameScene {
                addGestureFeedback(scene: gameScene, view: view)
            }
        }
    }
    
    func presentGameOverScene(for level: Int, in world: Int, with state: GameConstants.PlayerState = .IdleRight) {
        let playScreen: CGSize = view.safeAreaLayoutGuide.layoutFrame.size
        
        let scene = GameOverScene(size: CGSize(width: playScreen.width, height: playScreen.height), world: world, level: level, sceneManagerDelegate: self, state: state)
        scene.scaleMode = .aspectFit
        present(scene: scene)
    }
    
    func presentAchievementScene() {
        let playScreen: CGSize = view.safeAreaLayoutGuide.layoutFrame.size
        let scene = AchievementScene(size: playScreen, sceneManagerDelegate: self)
        scene.scaleMode = .aspectFit
        present(scene: scene)
    }
    
    func presentCreditsScene() {
        let playScreen: CGSize = view.safeAreaLayoutGuide.layoutFrame.size
        let scene = CreditsScene(size: playScreen, sceneManagerDelegate: self)
        scene.scaleMode = .aspectFit
        present(scene: scene)
    }
    
    func presentTransitionScene(nextLevel: Int, nextWorld: Int) {
        let playScreen: CGSize = view.safeAreaLayoutGuide.layoutFrame.size
        let scene = TransitionScene(size: playScreen, nextLevel: nextLevel, nextWorld: nextWorld,sceneManagerDelegate: self)
        scene.scaleMode = .aspectFit
        present(scene: scene)
    }
    
    func addGestureFeedback(scene: GameScene, view: SKView) {
        
        // Swipe to drag movement - player movement
        let panGesture = UIPanGestureRecognizer(target: scene, action: #selector(scene.playerGestureDetection))
        view.addGestureRecognizer(panGesture)
        
        let tapGesture = UITapGestureRecognizer(target: scene, action: #selector(scene.tapDetected))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
        
        // Long press is deprecated due to issue with users
        /*let longPress = UILongPressGestureRecognizer(target: scene, action: #selector(scene.longPressDetection))
        longPress.minimumPressDuration = 1.5
        view.addGestureRecognizer(longPress)*/
    }
}
